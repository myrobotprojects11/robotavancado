*** Settings ***
Resource        ../resources/Resource.robot
Resource        ../resources/pageobjects/SLATipoEntregaPage.robot
Resource        ../resources/pageobjects/HomePage.robot
Test Setup      Abrir navegador
Test Teardown   Fechar navegador

*** Test Cases ***
Caso de Teste 01: Validar se usuário consegui inputar Tipo da Entrega
    HomePage.Dado que usuário acessa a página Configurações - FMS/SLA - Tipo Entrega da Fast Shop Seller Center
    SLATipoEntregaPage.Quando usuário e inputar Tipo da Entrega
    SLATipoEntregaPage.Então o Tipo da Entrega inputado ficará evidente no textfield

Caso de Teste 02: Validar se usuário consegui inputar SLA (min)
    HomePage.Dado que usuário acessa a página Configurações - FMS/SLA - Tipo Entrega da Fast Shop Seller Center
    SLATipoEntregaPage.Quando usuário inputar SLA (min)
    SLATipoEntregaPage.Então o SLA (min) inputado ficará evidente no textfield

Caso de Teste 03: Validar se usuário consegui incluir Tipo da Entrega com sucesso
    HomePage.Dado que usuário acessa a página Configurações - FMS/SLA - Tipo Entrega da Fast Shop Seller Center
    SLATipoEntregaPage.Quando usuário preencher todos os campos obrigatórios e clicar no botão Incluir
    SLATipoEntregaPage.Então usuário irá incluir Tipo da Entrega com sucesso

Caso de Teste 04: Validar se apresenta o alerta de erro ao tentar criar um Tipo da Entrega com dados inconsistente 
    HomePage.Dado que usuário acessa a página Configurações - FMS/SLA - Tipo Entrega da Fast Shop Seller Center
    SLATipoEntregaPage.Quando usuário preencher os campos com dados inconsistente e clicar no botão Incluir
    SLATipoEntregaPage.Então usuário irá visualizar uma mensagem de alerta

Caso de Teste 05: Validar se usuário consegui visualizar modal Deletar
    HomePage.Dado que usuário acessa a página Configurações - FMS/SLA - Tipo Entrega da Fast Shop Seller Center
    SLATipoEntregaPage.Quando usuário selecionar Tipo da Entrega e clicar no botão Excluir
    SLATipoEntregaPage.Então usuário irá visualizar modal Deletar

Caso de Teste 06: Validar se usuário consegui fechar modal Deletar, após selecionar um Tipo da Entrega, clicar no botão Excluir em seguida no botão Cancelar 
    HomePage.Dado que usuário acessa a página Configurações - FMS/SLA - Tipo Entrega da Fast Shop Seller Center
    SLATipoEntregaPage.Quando usuário selecionar Tipo da Entrega, clicar no botão Excluir em seguida no botão Cancelar
    SLATipoEntregaPage.Então usuário irá fechar modal Deletar

Caso de Teste 07: Validar se usuário consegui Excluir um Tipo da Entrega com sucesso
    HomePage.Dado que usuário acessa a página Configurações - FMS/SLA - Tipo Entrega da Fast Shop Seller Center
    SLATipoEntregaPage.Quando usuário selecionar Tipo da Entrega, clicar no botão Excluir em seguida no botão OK
    SLATipoEntregaPage.Então usuário irá Excluir um Tipo da Entrega com sucesso

Caso de Teste 08: Validar se usuário consegui editar Tipo da Entrega com sucesso
    HomePage.Dado que usuário acessa a página Configurações - FMS/SLA - Tipo Entrega da Fast Shop Seller Center
    SLATipoEntregaPage.Quando usuário selecionar Tipo da Entrega, alterar dados em seguida clicar no botão Editar
    SLATipoEntregaPage.Então usuário irá Editar um Tipo da Entrega com sucesso

Caso de Teste 09: Validar se apresenta o alerta de erro ao tentar editar um Tipo da Entrega com dados inconsistente 
    HomePage.Dado que usuário acessa a página Configurações - FMS/SLA - Tipo Entrega da Fast Shop Seller Center
    SLATipoEntregaPage.Quando usuário selecionar Tipo da Entrega, alterar informações com dados inconsistente em seguida clicar no botão Editar
    SLATipoEntregaPage.Então usuário irá visualizar uma mensagem de alerta