*** Settings ***
Resource        ../resources/Resource.robot
Resource        ../resources/pageobjects/CadastroDeUsuariosFMSPage.robot
Resource        ../resources/pageobjects/HomePage.robot
Test Setup      Abrir navegador
Test Teardown   Fechar navegador

*** Test Cases ***
Caso de Teste 01: Validar se ao clicar no botão Criar Novo Perfil de Usuário, usuário consegui ser direcionado para a página de Cadastro Perfil de Usuário
    HomePage.Dado que usuário acessa a página Configurações - FMS/Cadastro de Usuários FMS da Fast Shop Seller Center
    CadastroDeUsuariosFMSPage.Quando usuário clicar no botão Criar Novo Perfil de Usuário
    CadastroDeUsuariosFMSPage.Então usuário será direcionado para a página de Cadastro Perfil de Usuário

Caso de Teste 02: Validar se usuário consegui inputar Usuário em Dados de Usuário
    HomePage.Dado que usuário acessa a página Configurações - FMS/Cadastro de Usuários FMS da Fast Shop Seller Center
    CadastroDeUsuariosFMSPage.Quando usuário clicar no botão Criar Novo Perfil de Usuário e inputar Usuário
    CadastroDeUsuariosFMSPage.Então o Usuário inputado ficará evidente no textfield

Caso de Teste 03: Validar se usuário consegui inputar Descrição em Dados de Usuário
    HomePage.Dado que usuário acessa a página Configurações - FMS/Cadastro de Usuários FMS da Fast Shop Seller Center
    CadastroDeUsuariosFMSPage.Quando usuário clicar no botão Criar Novo Perfil de Usuário e inputar Descrição
    CadastroDeUsuariosFMSPage.Então a Descrição inputado ficará evidente no textfield

Caso de Teste 04: Validar se usuário consegui selecionar uma opção no comboBox Seller
    HomePage.Dado que usuário acessa a página Configurações - FMS/Cadastro de Usuários FMS da Fast Shop Seller Center
    CadastroDeUsuariosFMSPage.Quando usuário clicar no botão Criar Novo Perfil de Usuário e selecionar uma opção no comboBox Seller
    CadastroDeUsuariosFMSPage.Então a opção selecionada no comboBox Seller ficará evidente 

Caso de Teste 05: Validar se usuário consegui selecionar uma opção no comboBox Webstore
    HomePage.Dado que usuário acessa a página Configurações - FMS/Cadastro de Usuários FMS da Fast Shop Seller Center
    CadastroDeUsuariosFMSPage.Quando usuário clicar no botão Criar Novo Perfil de Usuário e selecionar uma opção no comboBox Webstore
    CadastroDeUsuariosFMSPage.Então a opção selecionada no comboBox Webstore ficará evidente 

Caso de Teste 06: Validar se usuário consegui selecionar uma opção no comboBox Loja
    HomePage.Dado que usuário acessa a página Configurações - FMS/Cadastro de Usuários FMS da Fast Shop Seller Center
    CadastroDeUsuariosFMSPage.Quando usuário clicar no botão Criar Novo Perfil de Usuário e selecionar uma opção no comboBox Loja
    CadastroDeUsuariosFMSPage.Então a opção selecionada no comboBox Loja ficará evidente 

Caso de Teste 07: Validar se usuário consegui criar um usuário com sucesso
    HomePage.Dado que usuário acessa a página Configurações - FMS/Cadastro de Usuários FMS da Fast Shop Seller Center
    CadastroDeUsuariosFMSPage.Quando usuário clicar no botão Criar Novo Perfil de Usuário, preencher todos os campos obrigatórios e clicar no botão Salvar
    CadastroDeUsuariosFMSPage.Então usuário irá criar um usuário com sucesso

Caso de Teste 08: Validar se apresenta o alerta de erro ao tentar criar um usuário com dados inconsistente 
    HomePage.Dado que usuário acessa a página Configurações - FMS/Cadastro de Usuários FMS da Fast Shop Seller Center
    CadastroDeUsuariosFMSPage.Quando usuário clicar no botão Criar Novo Perfil de Usuário, preencher os campos com dados inconsistente e clicar no botão Salvar
    CadastroDeUsuariosFMSPage.Então usuário irá visualizar uma mensagem de alerta

Caso de Teste 09: Validar se usuário consegui editar um usuário com sucesso
    HomePage.Dado que usuário acessa a página Configurações - FMS/Cadastro de Usuários FMS da Fast Shop Seller Center
    CadastroDeUsuariosFMSPage.Quando usuário clicar no botão Editar, realizar alguma alteração em seguida clicar no botão Salvar
    CadastroDeUsuariosFMSPage.Então usuário irá conseguir editar um usuário com sucesso

Caso de Teste 10: Validar se usuário consegui visualizar modal Deletar
    HomePage.Dado que usuário acessa a página Configurações - FMS/Cadastro de Usuários FMS da Fast Shop Seller Center
    CadastroDeUsuariosFMSPage.Quando usuário clicar no botão Deletar
    CadastroDeUsuariosFMSPage.Então usuário irá conseguir visualizar modal Deletar

Caso de Teste 11: Validar se usuário consegui fechar modal Deletar, após clicar no botão Deletar em seguida no botão Cancelar 
    HomePage.Dado que usuário acessa a página Configurações - FMS/Cadastro de Usuários FMS da Fast Shop Seller Center
    CadastroDeUsuariosFMSPage.Quando usuário clicar no botão Deletar em seguida no botão Cancelar
    CadastroDeUsuariosFMSPage.Então usuário irá conseguir fechar a modal Deletar

Caso de Teste 12: Validar se usuário consegui deletar um perfil de usuário com sucesso
    HomePage.Dado que usuário acessa a página Configurações - FMS/Cadastro de Usuários FMS da Fast Shop Seller Center
    CadastroDeUsuariosFMSPage.Quando usuário clicar no botão Deletar em seguida no botão OK
    CadastroDeUsuariosFMSPage.Então usuário irá conseguir deletar um perfil de usuário com sucesso