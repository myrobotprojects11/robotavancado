*** Settings ***
Resource        ../resources/Resource.robot
Resource        ../resources/pageobjects/CadastroDeLojasPage.robot
Resource        ../resources/pageobjects/HomePage.robot
Test Setup      Abrir navegador
Test Teardown   Fechar navegador

*** Test Cases ***
Caso de Teste 01: Validar se usuário consegui ser direcionado para página Cadastro de Lojas, após clicar no botão Criar Nova Loja
    HomePage.Dado que usuário acessa a página Configurações - FMS/Cadastro de Lojas da Fast Shop Seller Center
    CadastroDeLojasPage.Quando usuário clicar no botão Criar Nova Loja
    CadastroDeLojasPage.Então usuário será direcionado para a página de Cadastro de Lojas

Caso de Teste 02: Validar se usuário consegui selecionar uma opção no comboBox Seller em Dados da Loja
    HomePage.Dado que usuário acessa a página Configurações - FMS/Cadastro de Lojas da Fast Shop Seller Center
    CadastroDeLojasPage.Quando clicar no botão Criar Nova Loja e selecionar uma opção no comboBox Seller
    CadastroDeLojasPage.Então a opção selecionada no comboBox Seller ficará evidente 

Caso de Teste 03: Validar se usuário consegui selecionar uma opção no comboBox WebStore em Dados da Loja
    HomePage.Dado que usuário acessa a página Configurações - FMS/Cadastro de Lojas da Fast Shop Seller Center
    CadastroDeLojasPage.Quando clicar no botão Criar Nova Loja e selecionar uma opção no comboBox WebStore
    CadastroDeLojasPage.Então a opção selecionada no comboBox WebStore ficará evidente 

Caso de Teste 04: Validar se usuário consegui inputar CNPJ 
    HomePage.Dado que usuário acessa a página Configurações - FMS/Cadastro de Lojas da Fast Shop Seller Center
    CadastroDeLojasPage.Quando usuário clicar no botão Criar Nova Loja e inputar CNPJ
    CadastroDeLojasPage.Então o CNPJ inputado ficará evidente no textfield

Caso de Teste 05: Validar se usuário consegui inputar Descrição
    HomePage.Dado que usuário acessa a página Configurações - FMS/Cadastro de Lojas da Fast Shop Seller Center
    CadastroDeLojasPage.Quando usuário clicar no botão Criar Nova Loja e inputar Descrição
    CadastroDeLojasPage.Então o Descrição inputado ficará evidente no textfield

Caso de Teste 06: Validar se usuário consegui inputar Filial
    HomePage.Dado que usuário acessa a página Configurações - FMS/Cadastro de Lojas da Fast Shop Seller Center
    CadastroDeLojasPage.Quando usuário clicar no botão Criar Nova Loja e inputar Filial
    CadastroDeLojasPage.Então o Filial inputado ficará evidente no textfield

Caso de Teste 07: Validar se usuário consegui inputar CEP
    HomePage.Dado que usuário acessa a página Configurações - FMS/Cadastro de Lojas da Fast Shop Seller Center
    CadastroDeLojasPage.Quando usuário clicar no botão Criar Nova Loja e inputar CEP
    CadastroDeLojasPage.Então o CEP inputado ficará evidente no textfield

Caso de Teste 08: Validar se ao inputar um CEP válido e clicar no ícone de pesquisa os campos de endereço são preenchidos com os dados do CEP digitado
    HomePage.Dado que usuário acessa a página Configurações - FMS/Cadastro de Lojas da Fast Shop Seller Center
    CadastroDeLojasPage.Quando usuário clicar no botão Criar Nova Loja, inputar um CEP válido e clicar no ícone de pesquisa
    CadastroDeLojasPage.Então os campos de endereço são preenchidos com os dados do CEP digitado

Caso de Teste 09: Validar se apresenta mensagem de Erro ao tentar inputar um CEP inválido 
    HomePage.Dado que usuário acessa a página Configurações - FMS/Cadastro de Lojas da Fast Shop Seller Center
    CadastroDeLojasPage.Quando usuário clicar no botão Criar Nova Loja, inputar um CEP inválido e clicar no ícone de pesquisa
    CadastroDeLojasPage.Então usuário irá visualizar uma mensagem de alerta

Caso de Teste 10: Validar se usuário consegui inputar Endereço
    HomePage.Dado que usuário acessa a página Configurações - FMS/Cadastro de Lojas da Fast Shop Seller Center
    CadastroDeLojasPage.Quando usuário clicar no botão Criar Nova Loja e inputar Endereço
    CadastroDeLojasPage.Então o Endereço inputado ficará evidente no textfield

Caso de Teste 11: Validar se usuário consegui inputar Número
    HomePage.Dado que usuário acessa a página Configurações - FMS/Cadastro de Lojas da Fast Shop Seller Center
    CadastroDeLojasPage.Quando usuário clicar no botão Criar Nova Loja e inputar Número
    CadastroDeLojasPage.Então o Número inputado ficará evidente no textfield

Caso de Teste 12: Validar se usuário consegui inputar Bairro
    HomePage.Dado que usuário acessa a página Configurações - FMS/Cadastro de Lojas da Fast Shop Seller Center
    CadastroDeLojasPage.Quando usuário clicar no botão Criar Nova Loja e inputar Bairro
    CadastroDeLojasPage.Então o Bairro inputado ficará evidente no textfield

Caso de Teste 13: Validar se usuário consegui inputar Cidade
    HomePage.Dado que usuário acessa a página Configurações - FMS/Cadastro de Lojas da Fast Shop Seller Center
    CadastroDeLojasPage.Quando usuário clicar no botão Criar Nova Loja e inputar Cidade
    CadastroDeLojasPage.Então a Cidade inputado ficará evidente no textfield

Caso de Teste 14: Validar se usuário consegui inputar UF
    HomePage.Dado que usuário acessa a página Configurações - FMS/Cadastro de Lojas da Fast Shop Seller Center
    CadastroDeLojasPage.Quando usuário clicar no botão Criar Nova Loja e inputar UF
    CadastroDeLojasPage.Então o UF inputado ficará evidente no textfield

Caso de Teste 15: Validar se usuário consegui inputar Complemento
    HomePage.Dado que usuário acessa a página Configurações - FMS/Cadastro de Lojas da Fast Shop Seller Center
    CadastroDeLojasPage.Quando usuário clicar no botão Criar Nova Loja e inputar Complemento
    CadastroDeLojasPage.Então o Complemento inputado ficará evidente no textfield

Caso de Teste 16: Validar se usuário consegui inputar Latitude
    HomePage.Dado que usuário acessa a página Configurações - FMS/Cadastro de Lojas da Fast Shop Seller Center
    CadastroDeLojasPage.Quando usuário clicar no botão Criar Nova Loja e inputar Latitude
    CadastroDeLojasPage.Então o Latitude inputado ficará evidente no textfield

Caso de Teste 17: Validar se usuário consegui inputar Longitude
    HomePage.Dado que usuário acessa a página Configurações - FMS/Cadastro de Lojas da Fast Shop Seller Center
    CadastroDeLojasPage.Quando usuário clicar no botão Criar Nova Loja e inputar Longitude
    CadastroDeLojasPage.Então o Longitude inputado ficará evidente no textfield

Caso de Teste 18: Validar se usuário consegui inputar ID da Loja
    HomePage.Dado que usuário acessa a página Configurações - FMS/Cadastro de Lojas da Fast Shop Seller Center
    CadastroDeLojasPage.Quando usuário clicar no botão Criar Nova Loja e inputar ID da Loja
    CadastroDeLojasPage.Então o ID da Loja inputado ficará evidente no textfield

Caso de Teste 19: Validar se usuário consegui inputar Instruções de Retirada
    HomePage.Dado que usuário acessa a página Configurações - FMS/Cadastro de Lojas da Fast Shop Seller Center
    CadastroDeLojasPage.Quando usuário clicar no botão Criar Nova Loja e inputar Instruções de Retirada
    CadastroDeLojasPage.Então as Instruções de Retirada inputado ficará evidente no textfield

Caso de Teste 20: Validar se usuário consegui efetuar um Cadastro de Lojas com sucesso
    HomePage.Dado que usuário acessa a página Configurações - FMS/Cadastro de Lojas da Fast Shop Seller Center
    CadastroDeLojasPage.Quando usuário clicar no botão Criar Nova Loja, preencher todos os campos corretamente e clicar no botão Salvar 
    CadastroDeLojasPage.Então usuário irá conseguir efetuar um Cadastro de Lojas com sucesso

Caso de Teste 21: Validar se usuário consegui Editar um Cadastro de Lojas com sucesso
    HomePage.Dado que usuário acessa a página Configurações - FMS/Cadastro de Lojas da Fast Shop Seller Center
    CadastroDeLojasPage.Quando usuário clicar no botão Editar, alterar informações do cadastro da loja e clicar no botão Salvar 
    CadastroDeLojasPage.Então usuário irá conseguir Editar um Cadastro de Lojas com sucesso

Caso de Teste 22: Validar se usuário consegui visualizar modal deletar
    HomePage.Dado que usuário acessa a página Configurações - FMS/Cadastro de Lojas da Fast Shop Seller Center
    CadastroDeLojasPage.Quando usuário clicar no botão Deletar
    CadastroDeLojasPage.Então usuário irá visualizar modal Deletar

Caso de Teste 23: Validar se usuário consegui fechar modal deletar 
    HomePage.Dado que usuário acessa a página Configurações - FMS/Cadastro de Lojas da Fast Shop Seller Center
    CadastroDeLojasPage.Quando usuário clicar no botão Deletar em seguida no botão Cancelar 
    CadastroDeLojasPage.Então usuário irá fechar modal Deletar

Caso de Teste 24: Validar se usuário consegui deletar com sucesso um Cadastro de Lojas
    HomePage.Dado que usuário acessa a página Configurações - FMS/Cadastro de Lojas da Fast Shop Seller Center
    CadastroDeLojasPage.Quando usuário clicar no botão Deletar em seguida no botão OK 
    CadastroDeLojasPage.Então usuário irá Excluir um Cadastro de Lojas com sucesso