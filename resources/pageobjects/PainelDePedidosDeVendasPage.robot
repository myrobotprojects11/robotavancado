*** Settings ***
Library     DateTime
Library     OperatingSystem
Library     SeleniumLibrary
Library     String
Library     FakerLibrary    locale=pt_BR

*** Variables ***
${PAINEL_DE_PEDIDOS_DE_VENDAS_URL}                      https://parceirofast-develop.fastshop.com.br/home/painel
${PAINEL_DE_PEDIDOS_DE_VENDAS_TITLE}                    Parceiro Fast | Painel de Pedidos de Vendas
${PAINEL_DE_PEDIDOS_DE_VENDAS_FILTRO}                   id:iconeFiltroBuscar
${PAINEL_DE_PEDIDOS_DE_VENDAS_FECHAR}                   id:iconeFechar
${PAINEL_DE_PEDIDOS_DE_VENDAS_COMBO_SELLER}             id:comboSeller
${PAINEL_DE_PEDIDOS_DE_VENDAS_COMBO_WEBSTORE}           id:comboWebStore
${PAINEL_DE_PEDIDOS_DE_VENDAS_COMBO_LOJAS}              id:comboStore
${PAINEL_DE_PEDIDOS_DE_VENDAS_DATA_INICIO}              id:dataInicio
${PAINEL_DE_PEDIDOS_DE_VENDAS_DATA_FIM}                 id:dataFim
${PAINEL_DE_PEDIDOS_DE_VENDAS_COMBO_STATUS}             id:comboStatus
${PAINEL_DE_PEDIDOS_DE_VENDAS_COMBO_PENDENCIAS}         id:comboPendencias
${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_CANCELAR}             id:btnCancelar
${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_CONFIRMAR}            id:btnConfirmar
${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_LIMPAR}               id:btnLimpar
${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_ATUALIZAR}            id:btnAtualizar
${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_BUSCAR}               id:btnBuscar
${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_MODAL_BUSCAR}         id:btnConsultar
${PAINEL_DE_PEDIDOS_DE_VENDAS_NUMERO_PEDIDO}            id:numeroPedido
${PAINEL_DE_PEDIDOS_DE_VENDAS_NOME_CLIENTE}             id:nomeCliente
${PAINEL_DE_PEDIDOS_DE_VENDAS_NOME_ENTREGADOR}          id:nomeEntregador
${PAINEL_DE_PEDIDOS_DE_VENDAS_DOCUMENTO}                id:documento
${PAINEL_DE_PEDIDOS_DE_VENDAS_PLACA}                    id:placaVeiculo
${PAINEL_DE_PEDIDOS_DE_VENDAS_CONTATO}                  id:contato
${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_EXPORTAR}             id:btnDownload
${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_MAPA_RASTREIO}        id:btnMapaRastreio
${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_COMPROVANTE_ENTREGA}  id:btnComprovanteEntrega
${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_ENTREGA_EFETIVA}      id:btnEntregaEfetiva
${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_DATA_ENTREGA}         id:dataEntrega
${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_HORA_ENTREGA}         id:horaEntrega
${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_SOLICITAR_COLETA}     id:btnSolicitarColeta
${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_CONFIRMAR_EXPEDICAO}  id:btnConfirmarExpericao
${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_MOTIVO_CANCELAMENTO}  id:btnMotivoCancelamento
${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_CONFIRMAR_PEDIDO}     id:btnConfirmarPedido
${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_REJEITAR_PEDIDO}      id:btnRejeitarPedido
${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_SALVAR}               id:btnSalvar
${PAINEL_DE_PEDIDOS_DE_VENDAS_TABLE}                    id:gridOrder
${FILE}                                                 C:\\Users\\DemetriusSoaresNasci\\Downloads\\RelatorioOrder.xlsx
${PAINEL_DE_PEDIDOS_DE_VENDAS_EM_ROTA_DE_ENTREGA}       xpath://h5[contains(.,'Em Rota de Entrega')]
${PAINEL_DE_PEDIDOS_DE_VENDAS_FECHAR_ENTREGA_EFETIVA}   id:iconeFecharBusca 


*** Keywords ***
#Caso de Teste 01
Quando usuário estiver na aba Painel de Pedidos de Vendas
    Sleep  2
    Title Should Be                ${PAINEL_DE_PEDIDOS_DE_VENDAS_TITLE}

Então usuário irá consegui visualizar o card Aguardando Confirmação
    Sleep  5
    Page Should Contain            Aguardando Confirmação

#Caso de Teste 02
Então usuário irá consegui visualizar o card Em Rota de Entrega
    Sleep  5
    Page Should Contain            Em Rota de Entrega

#Caso de Teste 03
Então usuário irá consegui visualizar o card Total de Pedidos
    Sleep  5
    Page Should Contain            Total de Pedidos

#Caso de Teste 04
Então usuário irá consegui visualizar o card Aguardando Pagamento
    Sleep  5
    Page Should Contain            Aguardando Pagamento

#Caso de Teste 05
Então usuário irá consegui visualizar o card Aguardando Separação
    Sleep  5
    Page Should Contain            Aguardando Separação

#Caso de Teste 06
Então usuário irá consegui visualizar o card Aguardando Expedição 
    Sleep  5
    Page Should Contain            Aguardando Expedição 

#Caso de Teste 07
Então usuário irá consegui visualizar o card Entregues
    Sleep  5
    Page Should Contain            Entregues

#Caso de Teste 08
Então usuário irá consegui visualizar o card Cancelados
    Sleep  5
    Page Should Contain            Cancelados

#Caso de Teste 09
Então usuário irá consegui visualizar o card Problemas Entrega
    Sleep  5
    Page Should Contain            Problemas Entrega

#Caso de Teste 10
Então usuário irá consegui visualizar o card Problemas Transportadora 
    Sleep  5
    Page Should Contain            Problemas Transportadora 

#Caso de Teste 11
Então usuário irá consegui visualizar o card Receita
    Sleep  5
    Page Should Contain            Receita

#Caso de Teste 12
Então usuário irá consegui visualizar o card Ticket Médio
    Sleep  5
    Page Should Contain            Ticket Médio

#Caso de Teste 13
Então usuário irá consegui visualizar o card Risco de Atraso
    Sleep  5
    Page Should Contain            Risco de Atraso

#Caso de Teste 14
Então usuário irá consegui visualizar o card Entregues com atraso
    Sleep  5
    Page Should Contain            Entregues com atraso

#Caso de Teste 15
Quando usuário clicar no ícone Filtro de Pedidos 
    Sleep  2
    Wait Until Element Is Not Visible  css:circle.ng-star-inserted  30sec
    Wait Until Element Is Visible  ${PAINEL_DE_PEDIDOS_DE_VENDAS_FILTRO}  30sec
    Click Element                  ${PAINEL_DE_PEDIDOS_DE_VENDAS_FILTRO}

Então usuário irá conseguir visualizar modal Filtros
    Sleep  2
    Page Should Contain            Filtros

#Caso de Teste 16
Quando usuário clicar no ícone Filtro de Pedidos em seguida no ícone de fechar (X)
    Sleep  2
    Wait Until Element Is Not Visible  css:circle.ng-star-inserted  30sec
    Wait Until Element Is Visible  ${PAINEL_DE_PEDIDOS_DE_VENDAS_FILTRO}  30sec
    Click Element                  ${PAINEL_DE_PEDIDOS_DE_VENDAS_FILTRO}
    Sleep  2
    Wait Until Element Is Visible  ${PAINEL_DE_PEDIDOS_DE_VENDAS_FECHAR}  30sec
    Click Element                  ${PAINEL_DE_PEDIDOS_DE_VENDAS_FECHAR}    

Então usuário irá conseguir fechar a modal Filtros
    Sleep  2
    Page Should Not Contain        Filtros

#Caso de Teste 17
Quando usuário clicar no ícone Filtro de Pedidos e selecionar uma opção no comboBox Seller
    Sleep  2
    Wait Until Element Is Visible  ${PAINEL_DE_PEDIDOS_DE_VENDAS_FILTRO}  30sec
    Click Element                  ${PAINEL_DE_PEDIDOS_DE_VENDAS_FILTRO}
    Sleep  2
    Wait Until Element Is Visible  ${PAINEL_DE_PEDIDOS_DE_VENDAS_COMBO_SELLER}  30sec
    Click Element                  ${PAINEL_DE_PEDIDOS_DE_VENDAS_COMBO_SELLER}
    Sleep  2
    Wait Until Element Is Visible  css:.mat-select-panel-wrap
    ${seller}=                     Get text  css:.mat-select-panel-wrap
    Log                            ${seller}
    Click Element                  xpath://span[contains(.,'FASTSHOP')] 

Então a opção selecionada no comboBox Seller ficará evidente 
    ${selected}=                   Get Text  ${PAINEL_DE_PEDIDOS_DE_VENDAS_COMBO_SELLER}
    Should Be Equal                ${selected}  FASTSHOP 

#Caso de Teste 18
Quando usuário clicar no ícone Filtro de Pedidos e selecionar uma opção no comboBox Web Store
    Sleep  2
    Wait Until Element Is Not Visible  css:circle.ng-star-inserted  30sec
    Wait Until Element Is Visible  ${PAINEL_DE_PEDIDOS_DE_VENDAS_FILTRO}  30sec
    Click Element                  ${PAINEL_DE_PEDIDOS_DE_VENDAS_FILTRO}
    Sleep  4
    Wait Until Element Is Visible  ${PAINEL_DE_PEDIDOS_DE_VENDAS_COMBO_SELLER}  30sec
    Click Element                  ${PAINEL_DE_PEDIDOS_DE_VENDAS_COMBO_SELLER}
    Sleep  7
    Wait Until Element Is Visible  css:.mat-select-panel-wrap
    Click Element                  xpath://span[contains(.,'FASTSHOP')] 
    Sleep  7
    Wait Until Element Is Visible  ${PAINEL_DE_PEDIDOS_DE_VENDAS_COMBO_WEBSTORE}  30sec
    Click Element                  ${PAINEL_DE_PEDIDOS_DE_VENDAS_COMBO_WEBSTORE}
    Sleep  2
    Wait Until Element Is Visible  css:.mat-select-panel-wrap
    ${webstore}=                   Get text  css:.mat-select-panel-wrap
    Log                            ${webstore}
    Click Element                  css=.mat-option-text

Então a opção selecionada no comboBox Web Store ficará evidente 
    ${selected}=                   Get Text  ${PAINEL_DE_PEDIDOS_DE_VENDAS_COMBO_WEBSTORE}
    Should Be Equal                ${selected}  FASTSHOP 

#Caso de Teste 19
Quando usuário clicar no ícone Filtro de Pedidos e selecionar uma opção no comboBox Lojas
    Sleep  2
    Wait Until Element Is Visible  ${PAINEL_DE_PEDIDOS_DE_VENDAS_FILTRO}  30sec
    Click Element                  ${PAINEL_DE_PEDIDOS_DE_VENDAS_FILTRO}
    Sleep  2
    Wait Until Element Is Visible  ${PAINEL_DE_PEDIDOS_DE_VENDAS_COMBO_LOJAS}  30sec
    Click Element                  ${PAINEL_DE_PEDIDOS_DE_VENDAS_COMBO_LOJAS}
    Sleep  2
    Wait Until Element Is Visible  css:.mat-select-panel-wrap
    ${lojas}=                      Get text  css:.mat-select-panel-wrap
    Log                            ${lojas}
    Click Element                  xpath://span[contains(.,'SAM1 - Samsung Shopping Paulista')] 

Então a opção selecionada no comboBox Lojas ficará evidente
    ${selected}=                   Get Text  ${PAINEL_DE_PEDIDOS_DE_VENDAS_COMBO_LOJAS}
    Should Be Equal                ${selected}  SAM1 - Samsung Shopping Paulista 

#Caso de Teste 20
Quando usuário clicar no ícone Filtro de Pedidos e inputar Data Início
    Sleep  2
    Wait Until Element Is Visible  ${PAINEL_DE_PEDIDOS_DE_VENDAS_FILTRO}  30sec
    Click Element                  ${PAINEL_DE_PEDIDOS_DE_VENDAS_FILTRO}
    Sleep  2
    ${DATE}                        Get Current Date  result_format=%d%m%y
    Set Test Variable              ${DATE}
    Sleep  2
    Wait Until Element Is Visible  ${PAINEL_DE_PEDIDOS_DE_VENDAS_DATA_INICIO}  30sec
    Input Text                     ${PAINEL_DE_PEDIDOS_DE_VENDAS_DATA_INICIO}  11032021
    
Então a Data Início inputada ficará evidente no textfield
    Sleep  2
    Textfield Should Contain       ${PAINEL_DE_PEDIDOS_DE_VENDAS_DATA_INICIO}  2021-03-11

#Caso de Teste 21
Quando usuário clicar no ícone Filtro de Pedidos e inputar Data Fim
    Sleep  2
    Wait Until Element Is Visible  ${PAINEL_DE_PEDIDOS_DE_VENDAS_FILTRO}  30sec
    Click Element                  ${PAINEL_DE_PEDIDOS_DE_VENDAS_FILTRO}
    Sleep  2
    ${DATE}                        Get Current Date  result_format=%d%m%y
    Set Test Variable              ${DATE}
    Sleep  2
    Wait Until Element Is Visible  ${PAINEL_DE_PEDIDOS_DE_VENDAS_DATA_FIM}  30sec
    Input Text                     ${PAINEL_DE_PEDIDOS_DE_VENDAS_DATA_FIM}  11032021

Então a Data Fim inputada ficará evidente no textfield
    Sleep  2
    Textfield Should Contain       ${PAINEL_DE_PEDIDOS_DE_VENDAS_DATA_FIM}  2021-03-11

#Caso de Teste 22
Quando usuário clicar no ícone Filtro de Pedidos e selecionar uma opção no comboBox Status
    Sleep  2
    Wait Until Element Is Visible  ${PAINEL_DE_PEDIDOS_DE_VENDAS_FILTRO}  30sec
    Click Element                  ${PAINEL_DE_PEDIDOS_DE_VENDAS_FILTRO}
    Sleep  2
    Wait Until Element Is Visible  ${PAINEL_DE_PEDIDOS_DE_VENDAS_COMBO_STATUS}  30sec
    Click Element                  ${PAINEL_DE_PEDIDOS_DE_VENDAS_COMBO_STATUS}
    Sleep  2
    Wait Until Element Is Visible  css:.mat-select-panel-wrap
    ${status}=                     Get text  css:.mat-select-panel-wrap
    Log                            ${Status}
    Click Element                  xpath://span[contains(.,'Aguardando Confirmação')] 

Então a opção selecionada no comboBox Status ficará evidente
    ${selected}=                   Get Text  ${PAINEL_DE_PEDIDOS_DE_VENDAS_COMBO_STATUS}
    Should Be Equal                ${selected}  Aguardando Confirmação 

#Caso de Teste 23
Quando usuário clicar no ícone Filtro de Pedidos e selecionar uma opção no comboBox Pendências
    Sleep  2
    Wait Until Element Is Visible  ${PAINEL_DE_PEDIDOS_DE_VENDAS_FILTRO}  30sec
    Click Element                  ${PAINEL_DE_PEDIDOS_DE_VENDAS_FILTRO}
    Sleep  2
    Wait Until Element Is Visible  ${PAINEL_DE_PEDIDOS_DE_VENDAS_COMBO_PENDENCIAS}  30sec
    Click Element                  ${PAINEL_DE_PEDIDOS_DE_VENDAS_COMBO_PENDENCIAS}
    Sleep  2
    Wait Until Element Is Visible  css:.mat-select-panel-wrap
    ${status}=                     Get text  css:.mat-select-panel-wrap
    Log                            ${Status}
    Click Element                  xpath://span[contains(.,'Sem Pendências')] 

Então a opção selecionada no comboBox Pendências ficará evidente
    ${selected}=                   Get Text  ${PAINEL_DE_PEDIDOS_DE_VENDAS_COMBO_PENDENCIAS}
    Should Be Equal                ${selected}  Sem Pendências 

#Caso de Teste 24
Quando usuário clicar no ícone Filtro de Pedidos em seguida no botão Cancelar 
    Sleep  2
    Wait Until Element Is Not Visible  css:circle.ng-star-inserted  30sec
    Wait Until Element Is Visible  ${PAINEL_DE_PEDIDOS_DE_VENDAS_FILTRO}  30sec
    Click Element                  ${PAINEL_DE_PEDIDOS_DE_VENDAS_FILTRO}
    Sleep  2
    Execute javascript             document.body.style.MozTransform = "scale(0.7)"
    Wait Until Element Is Visible  ${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_CANCELAR}  30sec
    Click Element                  ${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_CANCELAR}

Então usuário conseguirá fechar modal Filtros
    Sleep  2
    Page Should Not Contain        Filtros

#Caso de Teste 25
Quando usuário clicar no ícone Filtro de Pedidos, preencher os campos e em seguida no botão Limpar
    Sleep  2
    Wait Until Element Is Visible  ${PAINEL_DE_PEDIDOS_DE_VENDAS_FILTRO}  30sec
    Click Element                  ${PAINEL_DE_PEDIDOS_DE_VENDAS_FILTRO}
    Sleep  2
    Wait Until Element Is Visible  ${PAINEL_DE_PEDIDOS_DE_VENDAS_COMBO_SELLER}  30sec
    Click Element                  ${PAINEL_DE_PEDIDOS_DE_VENDAS_COMBO_SELLER}
    Sleep  2
    Wait Until Element Is Visible  xpath://span[contains(.,'FASTSHOP')]  30sec
    Click Element                  xpath://span[contains(.,'FASTSHOP')] 
    Sleep  2
    Wait Until Element Is Visible  ${PAINEL_DE_PEDIDOS_DE_VENDAS_COMBO_LOJAS}  30sec
    Click Element                  ${PAINEL_DE_PEDIDOS_DE_VENDAS_COMBO_LOJAS}
    Sleep  2
    Wait Until Element Is Visible  xpath://span[contains(.,'SAM3 - Shopping Tamboré')]  30sec 
    Click Element                  xpath://span[contains(.,'SAM3 - Shopping Tamboré')] 
    Sleep  2
    ${DATE}                        Get Current Date  result_format=%d%m%y
    Set Test Variable              ${DATE}
    Sleep  2
    Wait Until Element Is Visible  ${PAINEL_DE_PEDIDOS_DE_VENDAS_DATA_INICIO}  30sec
    Input Text                     ${PAINEL_DE_PEDIDOS_DE_VENDAS_DATA_INICIO}  11032021
    Sleep  2
    ${DATE}                        Get Current Date  result_format=%d%m%y
    Set Test Variable              ${DATE}
    Sleep  2
    Wait Until Element Is Visible  ${PAINEL_DE_PEDIDOS_DE_VENDAS_DATA_FIM}  30sec
    Input Text                     ${PAINEL_DE_PEDIDOS_DE_VENDAS_DATA_FIM}  11032021
    Sleep  2
    Wait Until Element Is Visible  ${PAINEL_DE_PEDIDOS_DE_VENDAS_COMBO_STATUS}  30sec
    Click Element                  ${PAINEL_DE_PEDIDOS_DE_VENDAS_COMBO_STATUS}
    Sleep  2
    Wait Until Element Is Visible  xpath://span[contains(.,'Aguardando Confirmação')]  30sec 
    Click Element                  xpath://span[contains(.,'Aguardando Confirmação')]
    Sleep  2
    Wait Until Element Is Visible  ${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_LIMPAR}  30sec 
    Click Element                  ${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_LIMPAR} 
    Sleep  3
    
Então usuário conseguirá limpar dados digitado nos campos
    Textfield Should Contain       ${PAINEL_DE_PEDIDOS_DE_VENDAS_DATA_INICIO}  ${EMPTY}

#Caso de Teste 26
Quando usuário clicar no botão Atualizar
    Sleep  2
    Wait Until Element Is Visible  ${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_ATUALIZAR}  30sec 
    Click Element                  ${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_ATUALIZAR}          

Então usuário irá consegui atualizar filtro de pedidos
    Sleep  5
    Page Should Contain Element    ${PAINEL_DE_PEDIDOS_DE_VENDAS_TABLE}

#Caso de Teste 27
Quando usuário clicar no botão Buscar
    Sleep  2
    Wait Until Element Is Visible  ${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_BUSCAR}  30sec 
    Click Element                  ${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_BUSCAR} 

#Caso de Teste 28
Quando usuário clicar no botão Buscar e digitar no textfield Número do Pedido 
    Sleep  2
    Wait Until Element Is Visible  ${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_BUSCAR}  30sec 
    Click Element                  ${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_BUSCAR}
    Sleep  2
    ${numero_pedido}=              generate random string  4  [NUMBERS]
    Set Test Variable              ${numero_pedido}
    Wait Until Element Is Visible  ${PAINEL_DE_PEDIDOS_DE_VENDAS_NUMERO_PEDIDO}  30sec 
    Input Text                     ${PAINEL_DE_PEDIDOS_DE_VENDAS_NUMERO_PEDIDO}  ${numero_pedido}

Então o Número do Pedido inputada ficará evidente no textfield
    Textfield Should Contain       ${PAINEL_DE_PEDIDOS_DE_VENDAS_NUMERO_PEDIDO}  ${numero_pedido}

#Caso de Teste 29
Quando usuário clicar no botão Buscar e digitar no textfield Nome de Cliente
    Sleep  2
    Wait Until Element Is Visible  ${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_BUSCAR}  30sec 
    Click Element                  ${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_BUSCAR}
    Sleep  2
    ${NOME}                        FakerLibrary.Name
    Set Test Variable              ${NOME}
    Wait Until Element Is Visible  ${PAINEL_DE_PEDIDOS_DE_VENDAS_NOME_CLIENTE}  30sec 
    Input Text                     ${PAINEL_DE_PEDIDOS_DE_VENDAS_NOME_CLIENTE}  ${NOME}

Então o Nome de Cliente inputada ficará evidente no textfield
    Textfield Should Contain       ${PAINEL_DE_PEDIDOS_DE_VENDAS_NOME_CLIENTE}  ${NOME}

#Caso de Teste 30
Quando usuário clicar no botão Buscar em seguida no ícone de fechar (X)
    Sleep  2
    Wait Until Element Is Visible  ${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_BUSCAR}  30sec 
    Click Element                  ${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_BUSCAR}
    Sleep  2
    Wait Until Element Is Visible  ${PAINEL_DE_PEDIDOS_DE_VENDAS_FECHAR}  30sec 
    Click Element                  ${PAINEL_DE_PEDIDOS_DE_VENDAS_FECHAR}

#Caso de Teste 31
Quando usuário clicar no botão Exportar em XLS
    Sleep  2
    Wait Until Element Is Visible  ${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_EXPORTAR}  30sec 
    Click Element                  ${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_EXPORTAR}

Então usuário irá conseguir realizar download do arquivo 
    File Should Exist              ${FILE}

#Caso de Teste 32
Então usuário irá consegui visualizar a tabela de pedidos
    Page Should Contain Element    ${PAINEL_DE_PEDIDOS_DE_VENDAS_TABLE}

#Caso de Teste 33
Quando usuário preencher todos os campos obrigatórios na modal Filtro de Pedidos e clicar em Confirmar 
    Sleep  2
    Wait Until Element Is Not Visible  css:circle.ng-star-inserted  30sec
    Scroll Element Into View       ${PAINEL_DE_PEDIDOS_DE_VENDAS_FILTRO} 
    Wait Until Element Is Visible  ${PAINEL_DE_PEDIDOS_DE_VENDAS_FILTRO}  30sec
    Click Element                  ${PAINEL_DE_PEDIDOS_DE_VENDAS_FILTRO}
    Sleep  2
    ${DATE}                        Get Current Date  result_format=%d%m%y
    Set Test Variable              ${DATE}
    Sleep  2
    Wait Until Element Is Visible  ${PAINEL_DE_PEDIDOS_DE_VENDAS_DATA_INICIO}  30sec
    Input Text                     ${PAINEL_DE_PEDIDOS_DE_VENDAS_DATA_INICIO}  08032021
    Sleep  2
    ${DATE}                        Get Current Date  result_format=%d%m%y
    Set Test Variable              ${DATE}
    Sleep  2
    Wait Until Element Is Visible  ${PAINEL_DE_PEDIDOS_DE_VENDAS_DATA_FIM}  30sec
    Input Text                     ${PAINEL_DE_PEDIDOS_DE_VENDAS_DATA_FIM}  08032021
    Sleep  2
    Execute javascript             document.body.style.zoom="67%"
    Sleep  2
    Press Keys                     ${PAINEL_DE_PEDIDOS_DE_VENDAS_DATA_FIM}  TAB
    
Então usuário irá conseguir visualizar na tabela de pedidos os dados do pedido informado na modal Filtro
    Sleep  7
    Table Row Should Contain       ${PAINEL_DE_PEDIDOS_DE_VENDAS_TABLE}  1  FASTSHOP

#Caso de Teste 34
Quando usuário preencher Número do Pedido e clicar em Buscar
    Sleep  2
    Wait Until Element Is Visible  ${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_BUSCAR}  30sec 
    Click Element                  ${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_BUSCAR}
    Sleep  2
    Wait Until Element Is Visible  ${PAINEL_DE_PEDIDOS_DE_VENDAS_NUMERO_PEDIDO}  30sec 
    Input Text                     ${PAINEL_DE_PEDIDOS_DE_VENDAS_NUMERO_PEDIDO}  CM9671964 
    Sleep  2
    Click Element                  ${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_MODAL_BUSCAR}

Então usuário irá consegui visualizar na tabela de pedidos os dados da Buscar pelo Número do Pedido
    Sleep  7
    Table Row Should Contain       ${PAINEL_DE_PEDIDOS_DE_VENDAS_TABLE}  1  CM9671964 

#Caso de Teste 35
Quando usuário preencher Nome de Cliente e clicar em Buscar
    Sleep  2
    Wait Until Element Is Visible  ${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_BUSCAR}  30sec 
    Click Element                  ${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_BUSCAR}
    Sleep  2
    Wait Until Element Is Visible  ${PAINEL_DE_PEDIDOS_DE_VENDAS_NOME_CLIENTE}  30sec 
    Input Text                     ${PAINEL_DE_PEDIDOS_DE_VENDAS_NOME_CLIENTE}  KARINA TOSCARO FEHRER 
    Sleep  2
    Click Element                  ${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_MODAL_BUSCAR}

Então usuário irá consegui visualizar na tabela de pedidos os dados da Buscar pelo Nome de Cliente
    Sleep  7
    Table Row Should Contain       ${PAINEL_DE_PEDIDOS_DE_VENDAS_TABLE}  1  CM9671964 

#Caso de Teste 36
Quando usuário clicar em algum pedido da tabela
    Sleep  2
    Wait Until Element Is Visible  xpath://td  30sec
    Click Element                  xpath://td 

Então usuário será direcionado para a página de Detalhamento do Pedido
    Sleep  5
    Page Should Contain            Detalhamento do Pedido

#Caso de Teste 37
Quando usuário clicar no ícone filtro e preencher dados inconsistente
    Sleep  2
    Wait Until Element Is Visible  ${PAINEL_DE_PEDIDOS_DE_VENDAS_FILTRO}  30sec
    Click Element                  ${PAINEL_DE_PEDIDOS_DE_VENDAS_FILTRO}
    Sleep  5
    Execute javascript             document.body.style.zoom="70%"
    ${DATE}                        Get Current Date  result_format=%d%m%y
    Set Test Variable              ${DATE}
    Sleep  5
    Wait Until Element Is Visible  ${PAINEL_DE_PEDIDOS_DE_VENDAS_DATA_INICIO}  30sec
    Input Text                     ${PAINEL_DE_PEDIDOS_DE_VENDAS_DATA_INICIO}  04/03/2021
    Sleep  2
    Wait Until Element Is Visible  ${PAINEL_DE_PEDIDOS_DE_VENDAS_DATA_FIM}  30sec
    Input Text                     ${PAINEL_DE_PEDIDOS_DE_VENDAS_DATA_FIM}  03/03/2021
    Sleep  2
    Press Keys                     ${PAINEL_DE_PEDIDOS_DE_VENDAS_DATA_FIM}  TAB

Então usuário irá visualizar uma mensagem de alerta 
    Sleep  10
    Page Should Contain Element    xpath://span[contains(.,'ERRO')]

#Caso de Teste 38
Quando usuário clicar no botão Buscar e efetuar uma buscar por Número de Pedido inconsistente
    Sleep  2
    Wait Until Element Is Visible  ${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_BUSCAR}  30sec
    Click Element                  ${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_BUSCAR}
    Sleep  5
    Wait Until Element Is Visible  ${PAINEL_DE_PEDIDOS_DE_VENDAS_NUMERO_PEDIDO}  30sec
    Input Text                     ${PAINEL_DE_PEDIDOS_DE_VENDAS_NUMERO_PEDIDO}  1111111
    Sleep  2
    Click Element                  ${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_MODAL_BUSCAR}
    Wait Until Element Is Not Visible  css:circle.ng-star-inserted  30sec

#Caso de Teste 39
Quando usuário clicar no botão Buscar e efetuar uma buscar por Nome de Cliente inconsistente
    Sleep  2
    Wait Until Element Is Visible  ${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_BUSCAR}  30sec
    Click Element                  ${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_BUSCAR}
    Sleep  5
    Wait Until Element Is Visible  ${PAINEL_DE_PEDIDOS_DE_VENDAS_NOME_CLIENTE}  30sec
    Input Text                     ${PAINEL_DE_PEDIDOS_DE_VENDAS_NOME_CLIENTE}  Demétrius Soares do Nascimento
    Sleep  2
    Click Element                  ${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_MODAL_BUSCAR}
    Wait Until Element Is Not Visible  css:circle.ng-star-inserted  30sec
    
#Caso de Teste 40
Quando usuário acessar a modal Filtros e digitar no textfield Nome do Entregador
    Sleep  2
    Wait Until Element Is Visible  xpath://td[contains(.,'Em rota de Entrega')]  30sec
    Click Element                  xpath://td[contains(.,'Em rota de Entrega')] 
    Sleep  4
    Wait Until Element Is Visible  ${PAINEL_DE_PEDIDOS_DE_VENDAS_NOME_ENTREGADOR}  30sec
    Run Keyword And Ignore Error   Input Text                     ${PAINEL_DE_PEDIDOS_DE_VENDAS_NOME_ENTREGADOR}  Demétrius Soares do Nascimento  

Então o Nome do Entregador inputada ficará evidente no textfield 
    Sleep  2
    Run Keyword And Ignore Error   Textfield Should Contain       ${PAINEL_DE_PEDIDOS_DE_VENDAS_NOME_ENTREGADOR}  Demétrius Soares do Nascimento  

#Caso de Teste 41
Quando usuário acessar a modal Filtros e digitar no textfield Documento
    Sleep  2
    Wait Until Element Is Visible  xpath://td[contains(.,'Em rota de Entrega')]  30sec
    Click Element                  xpath://td[contains(.,'Em rota de Entrega')] 
    Sleep  4
    Wait Until Element Is Visible  ${PAINEL_DE_PEDIDOS_DE_VENDAS_DOCUMENTO}  30sec
    Run Keyword And Ignore Error   Input Text                     ${PAINEL_DE_PEDIDOS_DE_VENDAS_DOCUMENTO}  999999999

Então o Documento inputada ficará evidente no textfield 
    Run Keyword And Ignore Error   Textfield Should Contain       ${PAINEL_DE_PEDIDOS_DE_VENDAS_DOCUMENTO}  999999999

#Caso de Teste 42
Quando usuário acessar a modal Filtros e digitar no textfield Placa de Veículo
    Sleep  2
    Wait Until Element Is Visible  xpath://td[contains(.,'Em rota de Entrega')]  30sec
    Click Element                  xpath://td[contains(.,'Em rota de Entrega')] 
    Sleep  4
    Wait Until Element Is Visible  ${PAINEL_DE_PEDIDOS_DE_VENDAS_PLACA}  30sec
    Run Keyword And Ignore Error   Input Text                     ${PAINEL_DE_PEDIDOS_DE_VENDAS_PLACA}  DED9999

Então a Placa de Veículo inputada ficará evidente no textfield 
    Run Keyword And Ignore Error   Textfield Should Contain       ${PAINEL_DE_PEDIDOS_DE_VENDAS_PLACA}  DED-9999

#Caso de Teste 43
Quando usuário acessar a página Detalhamento do Pedido e digitar no textfield Contato
    Sleep  2
    Wait Until Element Is Visible  xpath://td[contains(.,'Em rota de Entrega')]  30sec
    Click Element                  xpath://td[contains(.,'Em rota de Entrega')] 
    Sleep  4
    Wait Until Element Is Visible  ${PAINEL_DE_PEDIDOS_DE_VENDAS_CONTATO}  30sec
    Run Keyword And Ignore Error   Input Text                     ${PAINEL_DE_PEDIDOS_DE_VENDAS_CONTATO}  11999999999

Então o Contato inputada ficará evidente no textfield 
    Run Keyword And Ignore Error   Textfield Should Contain       ${PAINEL_DE_PEDIDOS_DE_VENDAS_CONTATO}  (11) 9 9999-9999

#Caso de Teste 44
Quando usuário acessar a página Detalhamento do Pedido com status Em Rota de Entrega
    Sleep  4
    Wait Until Element Is Visible  xpath://td[contains(.,'Em rota de Entrega')]  30sec
    Click Element                  xpath://td[contains(.,'Em rota de Entrega')]
    Sleep  4
    Scroll Element Into View       ${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_SALVAR}
    
Então o botão Mapa de Rastreio de Entrega deve apresentar habilitado para receber click 
    Sleep  2
    Element Should Be Enabled      ${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_MAPA_RASTREIO}

#Caso de Teste 45
Quando usuário acessar a página Detalhamento do Pedido com status Entregue
    Sleep  4
    Wait Until Element Is Visible  xpath://td[contains(.,'Entregue')]  30sec
    Click Element                  xpath://td[contains(.,'Entregue')] 
    Sleep  4
    Scroll Element Into View       ${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_SALVAR}

Então o botão Comprovante de Entrega deve apresentar habilitado para receber click 
    Sleep  2
    Element Should Be Enabled      ${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_COMPROVANTE_ENTREGA}

#Caso de Teste 46
Então o botão Entrega Efetiva deve apresentar habilitado para receber click
    Sleep  2
    Element Should Be Enabled      ${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_ENTREGA_EFETIVA}

#Caso de Teste 47
Quando usuário acessar a página Detalhamento do Pedido com status Em Rota de Entrega e clicar no botão Entrega Efetiva
    Sleep  4
    Wait Until Element Is Visible  xpath://td[contains(.,'Em rota de Entrega')]  30sec
    Click Element                  xpath://td[contains(.,'Em rota de Entrega')] 
    Sleep  4
    Wait Until Element Is Visible  ${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_ENTREGA_EFETIVA}  30sec
    Scroll Element Into View       ${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_ENTREGA_EFETIVA} 
    Click Element                  ${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_ENTREGA_EFETIVA} 

Então usuário irá conseguir visualizar modal Entrega Efetiva
    Page Should Contain            Entrega Efetiva

#Caso de Teste 48
Quando usuário acessar a página Detalhamento do Pedido com status Em Rota de Entrega, clicar no botão Entrega Efetiva em seguida no ícone de fechar (X) 
    Sleep  4
    Wait Until Element Is Visible  xpath://td[contains(.,'Em rota de Entrega')]  30sec
    Click Element                  xpath://td[contains(.,'Em rota de Entrega')] 
    Sleep  4
    Wait Until Element Is Visible  ${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_ENTREGA_EFETIVA}  30sec
    Scroll Element Into View       ${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_ENTREGA_EFETIVA} 
    Click Element                  ${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_ENTREGA_EFETIVA} 
    Sleep  2
    Wait Until Element Is Visible  ${PAINEL_DE_PEDIDOS_DE_VENDAS_FECHAR_ENTREGA_EFETIVA}  30sec
    Click Element                  ${PAINEL_DE_PEDIDOS_DE_VENDAS_FECHAR_ENTREGA_EFETIVA}

Então irá fechar a modal Entrega Efetiva
    Page Should Not Contain Element  xpath://span[contains(.,'Entrega Efetiva')]

#Caso de Teste 49
Quando usuário acessar a página Detalhamento do Pedido com status Em Rota de Entrega, clicar no botão Entrega Efetiva e inputar Data
    Sleep  4
    Wait Until Element Is Visible  xpath://td[contains(.,'Em rota de Entrega')]  30sec
    Click Element                  xpath://td[contains(.,'Em rota de Entrega')] 
    Sleep  4
    Wait Until Element Is Visible  ${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_ENTREGA_EFETIVA}  30sec
    Scroll Element Into View       ${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_ENTREGA_EFETIVA} 
    Click Element                  ${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_ENTREGA_EFETIVA} 
    Sleep  2
    Wait Until Element Is Visible  ${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_DATA_ENTREGA}  30sec
    Input Text                     ${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_DATA_ENTREGA}  09032021

Então a Data inputada ficará evidente no textfield 
    Textfield Should Contain       ${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_DATA_ENTREGA}  2021-03-09

#Caso de Teste 50
Quando usuário acessar a página Detalhamento do Pedido com status Em Rota de Entrega, clicar no botão Entrega Efetiva e inputar Hora
    Sleep  4
    Wait Until Element Is Visible  xpath://td[contains(.,'Em rota de Entrega')]  30sec
    Click Element                  xpath://td[contains(.,'Em rota de Entrega')] 
    Sleep  4
    Wait Until Element Is Visible  ${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_ENTREGA_EFETIVA}  30sec
    Scroll Element Into View       ${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_ENTREGA_EFETIVA} 
    Click Element                  ${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_ENTREGA_EFETIVA} 
    Sleep  2
    Wait Until Element Is Visible  ${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_HORA_ENTREGA}  30sec
    Input Text                     ${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_HORA_ENTREGA}  1154

Então a Hora inputada ficará evidente no textfield
    Textfield Should Contain       ${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_HORA_ENTREGA}  11:54

#Caso de Teste 51
Quando usuário acessar a página Detalhamento do Pedido com status Aguardando Separação
    Sleep  4
    Wait Until Element Is Visible  xpath://td[contains(.,'Aguardando Separação')]  30sec
    Click Element                  xpath://td[contains(.,'Aguardando Separação')] 
    Sleep  4
    Scroll Element Into View       ${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_SALVAR}

Então o botão Solicitar Coleta deve apresentar habilitado para receber click 
    Element Should Be Enabled      ${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_SOLICITAR_COLETA}

#Caso de Teste 52
Quando usuário acessar a página Detalhamento do Pedido com status Aguardando Expedição
    Sleep  4
    Wait Until Element Is Visible  xpath://td[contains(.,'Aguardando Expedição')]  30sec
    Click Element                  xpath://td[contains(.,'Aguardando Expedição')] 
    Sleep  4
    Scroll Element Into View       ${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_SALVAR}

Então o botão Confirmar Expedição deve apresentar habilitado para receber click 
    Element Should Be Enabled      ${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_CONFIRMAR_EXPEDICAO}

#Caso de Teste 53
Quando usuário acessar a página Detalhamento do Pedido com status Cancelado
    Sleep  4
    Wait Until Element Is Visible  xpath://td[contains(.,'Cancelado')]  30sec
    Click Element                  xpath://td[contains(.,'Cancelado')] 
    Sleep  4
    Scroll Element Into View       ${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_SALVAR}

Então o botão Motivo de Cancelamento deve apresentar habilitado para receber click 
    Element Should Be Enabled      ${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_MOTIVO_CANCELAMENTO}

#Caso de Teste 54
Quando usuário acessar a página Detalhamento do Pedido com status Aguardando Confirmação
    Sleep  4
    Wait Until Element Is Visible  xpath://td[contains(.,'Aguardando Confirmação')]  30sec
    Click Element                  xpath://td[contains(.,'Aguardando Confirmação')] 
    Sleep  4
    Scroll Element Into View       ${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_SALVAR}

Então o botão Confirmar Pedido deve apresentar habilitado para receber click 
    Element Should Be Enabled      ${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_CONFIRMAR_PEDIDO}

#Caso de Teste 55
Então o botão Rejeitar Pedido deve apresentar habilitado para receber click 
    Element Should Be Enabled      ${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_REJEITAR_PEDIDO}

#Caso de Teste 56
Quando usuário acessar a página Detalhamento do Pedido
    Sleep  4
    Wait Until Element Is Visible  xpath://td[contains(.,'Aguardando Confirmação')]  30sec
    Click Element                  xpath://td[contains(.,'Aguardando Confirmação')] 
    Sleep  4
    Scroll Element Into View       ${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_SALVAR}

Então o botão Salvar deve apresentar habilitado para receber click 
    Element Should Be Enabled      ${PAINEL_DE_PEDIDOS_DE_VENDAS_BTN_SALVAR}


