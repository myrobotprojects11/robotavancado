*** Settings ***
Library     DateTime
Library     OperatingSystem
Library     SeleniumLibrary
Library     String
Library     FakerLibrary    locale=pt_BR

*** Variables ***
${CADASTRO_DE_USUARIOS_URL}                      https://parceirofast-develop.fastshop.com.br/home/painel
${CADASTRO_DE_USUARIOS_TITLE}                    Parceiro Fast | Cadastro de Usuários FMS
${CADASTRO_DE_USUARIOS_BTN_CRIAR_NOVO_PERFIL}    id:btnNovoUsuario
${CADASTRO_DE_USUARIOS_TEXTFIELD_USUARIO}        id:emailUsuario
${CADASTRO_DE_USUARIOS_TEXTFIELD_DESCRICAO}      id:nomeUsuario
${CADASTRO_DE_USUARIOS_COMBO_SELLER}             id:comboSeller
${CADASTRO_DE_USUARIOS_COMBO_WEBSTORE}           id:comboWebStore
${CADASTRO_DE_USUARIOS_COMBO_LOJA}               id:comboStore
${CADASTRO_DE_USUARIOS_BTN_SALVAR}               id:btnSalvarUsuario
${CADASTRO_DE_USUARIOS_BTN_EDITAR}               id:btnEditar
${CADASTRO_DE_USUARIOS_BTN_DELETAR}              id:btnExcluir
${CADASTRO_DE_USUARIOS_BTN_CANCELAR}             id:buttonSubmitCancel
${CADASTRO_DE_USUARIOS_BTN_APAGAR}               id:buttonSubmitApagar

*** Keywords ***
#Caso de Teste 01
Quando usuário clicar no botão Criar Novo Perfil de Usuário
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_USUARIOS_BTN_CRIAR_NOVO_PERFIL}  30sec
    Click Element                  ${CADASTRO_DE_USUARIOS_BTN_CRIAR_NOVO_PERFIL}

Então usuário será direcionado para a página de Cadastro Perfil de Usuário
    Sleep  5
    Page Should Contain            Cadastro Perfil de Usuário

#Caso de Teste 02
Quando usuário clicar no botão Criar Novo Perfil de Usuário e inputar Usuário
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_USUARIOS_BTN_CRIAR_NOVO_PERFIL}  30sec
    Click Element                  ${CADASTRO_DE_USUARIOS_BTN_CRIAR_NOVO_PERFIL}
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_USUARIOS_TEXTFIELD_USUARIO}  30sec
    ${EMAIL}                        FakerLibrary.Email
    Set Test Variable              ${EMAIL}
    Input Text                     ${CADASTRO_DE_USUARIOS_TEXTFIELD_USUARIO}  ${EMAIL}  

Então o Usuário inputado ficará evidente no textfield
    Textfield Should Contain       ${CADASTRO_DE_USUARIOS_TEXTFIELD_USUARIO}  ${EMAIL}

#Caso de Teste 03
Quando usuário clicar no botão Criar Novo Perfil de Usuário e inputar Descrição
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_USUARIOS_BTN_CRIAR_NOVO_PERFIL}  30sec
    Click Element                  ${CADASTRO_DE_USUARIOS_BTN_CRIAR_NOVO_PERFIL}
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_USUARIOS_TEXTFIELD_DESCRICAO}  30sec
    ${NOME}                        FakerLibrary.Name
    Set Test Variable              ${NOME}
    Input Text                     ${CADASTRO_DE_USUARIOS_TEXTFIELD_DESCRICAO}  ${NOME}  

Então a Descrição inputado ficará evidente no textfield
    Textfield Should Contain       ${CADASTRO_DE_USUARIOS_TEXTFIELD_DESCRICAO}  ${NOME}

#Caso de Teste 04
Quando usuário clicar no botão Criar Novo Perfil de Usuário e selecionar uma opção no comboBox Seller
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_USUARIOS_BTN_CRIAR_NOVO_PERFIL}  30sec
    Click Element                  ${CADASTRO_DE_USUARIOS_BTN_CRIAR_NOVO_PERFIL}
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_USUARIOS_COMBO_SELLER}  30sec
    Click Element                  ${CADASTRO_DE_USUARIOS_COMBO_SELLER}
    Sleep  2
    Wait Until Element Is Visible  css:.mat-select-panel-wrap
    ${seller}=                     Get text  css:.mat-select-panel-wrap
    Log                            ${seller}
    Click Element                  xpath://span[contains(.,'FASTSHOP ')] 

Então a opção selecionada no comboBox Seller ficará evidente 
    ${selected}=                   Get Text  ${CADASTRO_DE_USUARIOS_COMBO_SELLER}
    Should Be Equal                ${selected}  FASTSHOP  

#Caso de Teste 05
Quando usuário clicar no botão Criar Novo Perfil de Usuário e selecionar uma opção no comboBox Webstore
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_USUARIOS_BTN_CRIAR_NOVO_PERFIL}  30sec
    Click Element                  ${CADASTRO_DE_USUARIOS_BTN_CRIAR_NOVO_PERFIL}
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_USUARIOS_COMBO_SELLER}  30sec
    Click Element                  ${CADASTRO_DE_USUARIOS_COMBO_SELLER}
    Sleep  2
    Wait Until Element Is Visible  css:.mat-select-panel-wrap
    Click Element                  xpath://span[contains(.,'FASTSHOP ')] 
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_USUARIOS_COMBO_WEBSTORE}  30sec
    Click Element                  ${CADASTRO_DE_USUARIOS_COMBO_WEBSTORE}
    Sleep  2
    Wait Until Element Is Visible  css:.mat-select-panel-wrap
    ${seller}=                     Get text  css:.mat-select-panel-wrap
    Log                            ${seller}
    Click Element                  css=.mat-option-text
    Sleep  2

Então a opção selecionada no comboBox Webstore ficará evidente 
    ${selected}=                   Get Text  ${CADASTRO_DE_USUARIOS_COMBO_SELLER}
    Should Be Equal                ${selected}  FASTSHOP  

#Caso de Teste 06
Quando usuário clicar no botão Criar Novo Perfil de Usuário e selecionar uma opção no comboBox Loja
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_USUARIOS_BTN_CRIAR_NOVO_PERFIL}  30sec
    Click Element                  ${CADASTRO_DE_USUARIOS_BTN_CRIAR_NOVO_PERFIL}
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_USUARIOS_COMBO_SELLER}  30sec
    Click Element                  ${CADASTRO_DE_USUARIOS_COMBO_SELLER}
    Sleep  2
    Wait Until Element Is Visible  css:.mat-select-panel-wrap
    Click Element                  xpath://span[contains(.,'FASTSHOP ')] 
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_USUARIOS_COMBO_WEBSTORE}  30sec
    Click Element                  ${CADASTRO_DE_USUARIOS_COMBO_WEBSTORE}
    Sleep  2
    Click Element                  css=.mat-option-text
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_USUARIOS_COMBO_LOJA}  30sec
    Click Element                  ${CADASTRO_DE_USUARIOS_COMBO_LOJA}
    Sleep  2
    Wait Until Element Is Visible  css:.mat-select-panel-wrap
    ${seller}=                     Get text  css:.mat-select-panel-wrap
    Log                            ${seller}
    Click Element                  xpath://span[contains(.,'01 - ZAKI NARCHI')] 
    
Então a opção selecionada no comboBox Loja ficará evidente 
    ${selected}=                   Get Text  ${CADASTRO_DE_USUARIOS_COMBO_LOJA}
    Should Be Equal                ${selected}  01 - ZAKI NARCHI   

#Caso de Teste 07
Quando usuário clicar no botão Criar Novo Perfil de Usuário, preencher todos os campos obrigatórios e clicar no botão Salvar
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_USUARIOS_BTN_CRIAR_NOVO_PERFIL}  30sec
    Click Element                  ${CADASTRO_DE_USUARIOS_BTN_CRIAR_NOVO_PERFIL}
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_USUARIOS_TEXTFIELD_USUARIO}  30sec
    ${EMAIL}                        FakerLibrary.Email
    Set Test Variable              ${EMAIL}
    Input Text                     ${CADASTRO_DE_USUARIOS_TEXTFIELD_USUARIO}  ${EMAIL}  
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_USUARIOS_TEXTFIELD_DESCRICAO}  30sec
    ${NOME}                        FakerLibrary.Name
    Set Test Variable              ${NOME}
    Input Text                     ${CADASTRO_DE_USUARIOS_TEXTFIELD_DESCRICAO}  ${NOME}  
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_USUARIOS_COMBO_SELLER}  30sec
    Click Element                  ${CADASTRO_DE_USUARIOS_COMBO_SELLER}
    Sleep  2
    Wait Until Element Is Visible  css:.mat-select-panel-wrap
    Click Element                  xpath://span[contains(.,'FASTSHOP ')] 
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_USUARIOS_COMBO_WEBSTORE}  30sec
    Click Element                  ${CADASTRO_DE_USUARIOS_COMBO_WEBSTORE}
    Sleep  2
    Click Element                  css=.mat-option-text
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_USUARIOS_COMBO_LOJA}  30sec
    Click Element                  ${CADASTRO_DE_USUARIOS_COMBO_LOJA}
    Sleep  2
    Wait Until Element Is Visible  css:.mat-select-panel-wrap
    ${seller}=                     Get text  css:.mat-select-panel-wrap
    Log                            ${seller}
    Click Element                  xpath://span[contains(.,'01 - ZAKI NARCHI')] 
    Sleep  2
    Click Element                  ${CADASTRO_DE_USUARIOS_BTN_SALVAR}    

Então usuário irá criar um usuário com sucesso
    Wait Until Element Is Visible  xpath://span[contains(.,'SUCESSO')]  30sec
    Page Should Contain Element    xpath://span[contains(.,'SUCESSO')]

#Caso de Teste 08
Quando usuário clicar no botão Criar Novo Perfil de Usuário, preencher os campos com dados inconsistente e clicar no botão Salvar
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_USUARIOS_BTN_CRIAR_NOVO_PERFIL}  30sec
    Click Element                  ${CADASTRO_DE_USUARIOS_BTN_CRIAR_NOVO_PERFIL}
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_USUARIOS_BTN_SALVAR}
    Click Element                  ${CADASTRO_DE_USUARIOS_BTN_SALVAR}

Então usuário irá visualizar uma mensagem de alerta
    Wait Until Element Is Visible  xpath://span[contains(.,'ERRO')]  30sec
    Page Should Contain Element    xpath://span[contains(.,'ERRO')]

#Caso de Teste 09
Quando usuário clicar no botão Editar, realizar alguma alteração em seguida clicar no botão Salvar
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_USUARIOS_BTN_EDITAR}  30sec
    Click Element                  ${CADASTRO_DE_USUARIOS_BTN_EDITAR}
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_USUARIOS_BTN_SALVAR}  30sec
    Click Element                  ${CADASTRO_DE_USUARIOS_BTN_SALVAR}

Então usuário irá conseguir editar um usuário com sucesso
    Wait Until Element Is Visible  xpath://span[contains(.,'SUCESSO')]  30sec
    Page Should Contain Element    xpath://span[contains(.,'SUCESSO')]

#Caso de Teste 10
Quando usuário clicar no botão Deletar
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_USUARIOS_BTN_DELETAR}  30sec
    Click Element                  ${CADASTRO_DE_USUARIOS_BTN_DELETAR}
   
Então usuário irá conseguir visualizar modal Deletar
    Sleep  5
    Page Should Contain            Apagar um perfil de usuário

#Caso de Teste 11
Quando usuário clicar no botão Deletar em seguida no botão Cancelar
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_USUARIOS_BTN_DELETAR}  30sec
    Click Element                  ${CADASTRO_DE_USUARIOS_BTN_DELETAR}
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_USUARIOS_BTN_CANCELAR}  30sec
    Click Element                  ${CADASTRO_DE_USUARIOS_BTN_CANCELAR}

Então usuário irá conseguir fechar a modal Deletar
    Sleep  5
    Page Should Not Contain        Apagar um perfil de usuário

#Caso de Teste 12
Quando usuário clicar no botão Deletar em seguida no botão OK
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_USUARIOS_BTN_DELETAR}  30sec
    Click Element                  ${CADASTRO_DE_USUARIOS_BTN_DELETAR}
    Sleep  2
    Wait Until Element Is Visible  ${CADASTRO_DE_USUARIOS_BTN_APAGAR}  30sec
    Click Element                  ${CADASTRO_DE_USUARIOS_BTN_APAGAR}

Então usuário irá conseguir deletar um perfil de usuário com sucesso
    Wait Until Element Is Visible  xpath://span[contains(.,'SUCESSO')]  30sec
    Page Should Contain Element    xpath://span[contains(.,'SUCESSO')]


