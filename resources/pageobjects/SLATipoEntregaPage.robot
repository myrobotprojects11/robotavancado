*** Settings ***
Library     DateTime
Library     OperatingSystem
Library     SeleniumLibrary
Library     String
Library     FakerLibrary    locale=pt_BR
Resource    ../pageobjects/HomePage.robot

*** Variables ***
${SLA_TIPO_ENTREGA_URL}                        https://parceirofast-develop.fastshop.com.br/home/painel
${SLA_TIPO_ENTREGA_TITLE}                      Parceiro Fast | SLA - Tipo Entrega
${SLA_TIPO_ENTREGA_TEXTFIELD_TIPO_DA_ENTREGA}  id:tipoEntrega
${SLA_TIPO_ENTREGA_TEXTFIELD_SLA_MIN}          id:slaMin
${SLA_TIPO_ENTREGA_BTN_INCLUIR}                id:btnIncluir
${SLA_TIPO_ENTREGA_BTN_EXCLUIR}                id:btnExcluir
${SLA_TIPO_ENTREGA_BTN_EDITAR}                 id:btnEditar
${SLA_TIPO_ENTREGA_TABLE}                      id:gridTipoEntrega
${SLA_TIPO_ENTREGA_BTN_CANCELAR}               id:buttonSubmitCancel
${SLA_TIPO_ENTREGA_BTN_APAGAR}                 id:buttonSubmitApagar

*** Keywords ***
#Caso de Teste 01
Quando usuário e inputar Tipo da Entrega
    Sleep  2
    Wait Until Element Is Visible  ${SLA_TIPO_ENTREGA_TEXTFIELD_TIPO_DA_ENTREGA}  30sec
    ${tipo_entrega}                FakerLibrary.Job
    Set Test Variable              ${tipo_entrega}
    Input Text                     ${SLA_TIPO_ENTREGA_TEXTFIELD_TIPO_DA_ENTREGA}  ${tipo_entrega}

Então o Tipo da Entrega inputado ficará evidente no textfield
    Textfield Should Contain       ${SLA_TIPO_ENTREGA_TEXTFIELD_TIPO_DA_ENTREGA}  ${tipo_entrega}

#Caso de Teste 02
Quando usuário inputar SLA (min)
    Sleep  2
    Wait Until Element Is Visible  ${SLA_TIPO_ENTREGA_TEXTFIELD_SLA_MIN}  30sec
    ${SLA_MIN}=                    generate random string  2  [NUMBERS]
    Set Test Variable              ${SLA_MIN}
    Input Text                     ${SLA_TIPO_ENTREGA_TEXTFIELD_SLA_MIN}  ${SLA_MIN}

Então o SLA (min) inputado ficará evidente no textfield
    Textfield Should Contain       ${SLA_TIPO_ENTREGA_TEXTFIELD_SLA_MIN}  ${SLA_MIN}

#Caso de Teste 03
Quando usuário preencher todos os campos obrigatórios e clicar no botão Incluir
    Sleep  2
    Wait Until Element Is Visible  ${SLA_TIPO_ENTREGA_TEXTFIELD_TIPO_DA_ENTREGA}  30sec
    ${tipo_entrega}                FakerLibrary.Job
    Set Test Variable              ${tipo_entrega}
    Input Text                     ${SLA_TIPO_ENTREGA_TEXTFIELD_TIPO_DA_ENTREGA}  ${tipo_entrega}
    Sleep  2
    Wait Until Element Is Visible  ${SLA_TIPO_ENTREGA_TEXTFIELD_SLA_MIN}  30sec
    ${SLA_MIN}=                    generate random string  2  [NUMBERS]
    Set Test Variable              ${SLA_MIN}
    Input Text                     ${SLA_TIPO_ENTREGA_TEXTFIELD_SLA_MIN}  ${SLA_MIN}
    Wait Until Element Is Visible  ${SLA_TIPO_ENTREGA_BTN_INCLUIR}  
    Click Element                  ${SLA_TIPO_ENTREGA_BTN_INCLUIR}  
    
Então usuário irá incluir Tipo da Entrega com sucesso
    Sleep  5
    Page Should Contain Element    xpath://span[contains(.,'SUCESSO')]

#Caso de Teste 04
Quando usuário preencher os campos com dados inconsistente e clicar no botão Incluir
    Sleep  2
    Wait Until Element Is Visible  ${SLA_TIPO_ENTREGA_BTN_INCLUIR}  
    Click Element                  ${SLA_TIPO_ENTREGA_BTN_INCLUIR} 

Então usuário irá visualizar uma mensagem de alerta
    Wait Until Element Is Visible  xpath://span[contains(.,'ERRO')]  30sec
    Page Should Contain Element    xpath://span[contains(.,'ERRO')]

#Caso de Teste 05
Quando usuário selecionar Tipo da Entrega e clicar no botão Excluir
    Sleep  2
    Wait Until Element Is Visible  xpath://td[contains(.,'test')]  30sec
    Click Element                  xpath://td[contains(.,'test')] 
    Sleep  2
    Wait Until Element Is Visible  ${SLA_TIPO_ENTREGA_BTN_EXCLUIR}
    Click Element                  ${SLA_TIPO_ENTREGA_BTN_EXCLUIR}
    Sleep  5
    
    
Então usuário irá visualizar modal Deletar
    Page Should Contain            Apagar sla tipo de entrega    
        
#Caso de Teste 06
Quando usuário selecionar Tipo da Entrega, clicar no botão Excluir em seguida no botão Cancelar
    Sleep  2
    Wait Until Element Is Visible  xpath://td[contains(.,'test')]  30sec
    Click Element                  xpath://td[contains(.,'test')] 
    Sleep  2
    Wait Until Element Is Visible  ${SLA_TIPO_ENTREGA_BTN_EXCLUIR}
    Click Element                  ${SLA_TIPO_ENTREGA_BTN_EXCLUIR}
    Sleep  5
    Wait Until Element Is Visible  ${SLA_TIPO_ENTREGA_BTN_CANCELAR}
    Click Element                  ${SLA_TIPO_ENTREGA_BTN_CANCELAR}

Então usuário irá fechar modal Deletar
    Sleep  5
    Page Should Not Contain        Apagar sla tipo de entrega    

#Caso de Teste 07
Quando usuário selecionar Tipo da Entrega, clicar no botão Excluir em seguida no botão OK
    Sleep  2
    Wait Until Element Is Visible  xpath://td[contains(.,'test')]  30sec
    Click Element                  xpath://td[contains(.,'test')] 
    Sleep  2
    Wait Until Element Is Visible  ${SLA_TIPO_ENTREGA_BTN_EXCLUIR}
    Click Element                  ${SLA_TIPO_ENTREGA_BTN_EXCLUIR}
    Sleep  2
    Wait Until Element Is Visible  ${SLA_TIPO_ENTREGA_BTN_APAGAR}
    Click Element                  ${SLA_TIPO_ENTREGA_BTN_APAGAR}
    Sleep  3

Então usuário irá Excluir um Tipo da Entrega com sucesso
    Wait Until Element Is Visible  xpath://span[contains(.,'SUCESSO')]  30sec
    Page Should Contain Element    xpath://span[contains(.,'SUCESSO')]

#Caso de Teste 08
Quando usuário selecionar Tipo da Entrega, alterar dados em seguida clicar no botão Editar
    Sleep  2
    Wait Until Element Is Visible  xpath://td[contains(.,'Ultra FAST')]  30sec
    Click Element                  xpath://td[contains(.,'Ultra FAST')] 
    Sleep  2
    Wait Until Element Is Visible  ${SLA_TIPO_ENTREGA_BTN_EDITAR}
    Click Element                  ${SLA_TIPO_ENTREGA_BTN_EDITAR}
    
Então usuário irá Editar um Tipo da Entrega com sucesso
    Wait Until Element Is Visible  xpath://span[contains(.,'SUCESSO')]  30sec
    Page Should Contain Element    xpath://span[contains(.,'SUCESSO')]

#Caso de Teste 09
Quando usuário selecionar Tipo da Entrega, alterar informações com dados inconsistente em seguida clicar no botão Editar
    Sleep  2
    Wait Until Element Is Visible  xpath://td[contains(.,'Ultra FAST')]  30sec
    Click Element                  xpath://td[contains(.,'Ultra FAST')] 
    Sleep  2
    Clear Element Text             ${SLA_TIPO_ENTREGA_TEXTFIELD_SLA_MIN}
    Sleep  2
    Wait Until Element Is Visible  ${SLA_TIPO_ENTREGA_BTN_EDITAR}
    Click Element                  ${SLA_TIPO_ENTREGA_BTN_EDITAR}
    Sleep  5



