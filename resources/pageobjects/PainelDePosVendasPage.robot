*** Settings ***
Library     DateTime
Library     OperatingSystem
Library     SeleniumLibrary
Library     String
Library     FakerLibrary    locale=pt_BR

*** Variables ***
${PAINEL_DE_POS_VENDAS_URL}                             https://parceirofast-develop.fastshop.com.br/home/painel
${PAINEL_DE_POS_VENDAS_TITLE}                           Parceiro Fast | Painel de Pós Vendas
${PAINEL_DE_POS_VENDAS_FILTRO}                          id:iconeFiltroBuscar
${PAINEL_DE_POS_VENDAS_FECHAR}                          id:iconeFechar
${PAINEL_DE_POS_VENDAS_BTN_LIMPAR}                      id:btnLimpar
${PAINEL_DE_POS_VENDAS_COMBO_SELLER}                    id:comboSeller
${PAINEL_DE_POS_VENDAS_COMBO_WEBSTORE}                  id:comboWebStore
${PAINEL_DE_POS_VENDAS_COMBO_LOJAS}                     id:comboStore
${PAINEL_DE_POS_VENDAS_DATA_INICIO}                     id:dataInicial
${PAINEL_DE_POS_VENDAS_DATA_FIM}                        id:dataFinal
${PAINEL_DE_POS_VENDAS_COMBO_STATUS}                    id:comboStatus
${PAINEL_DE_POS_VENDAS_BTN_ATUALIZAR}                   id:btnAtualizar
${PAINEL_DE_POS_VENDAS_BTN_BUSCAR}                      id:btnFiltroBuscar
${PAINEL_DE_POS_VENDAS_NUMERO_SOLICITACAO}              id:numeroSolicitacao
${PAINEL_DE_POS_VENDAS_NUMERO_PEDIDO}                   id:numeroPedido
${PAINEL_DE_POS_VENDAS_NOME_CLIENTE}                    id:nomeCliente
${PAINEL_DE_POS_VENDAS_TABLE}                           id:gridOrderCancel
${PAINEL_DE_POS_VENDAS_BTN_MODAL_BUSCAR}                id:btnBuscar
${PAINEL_DE_POS_VENDAS_BTN_ABRIR_NOVA_SOLICITACAO}      id:btnNovaSolicitacao
${PAINEL_DE_POS_VENDAS_COMBO_TIPO_SOLICITACAO}          id:comboTipoRequisicao
${PAINEL_DE_POS_VENDAS_COMBO_MOTIVO}                    id:motivo
${PAINEL_DE_POS_VENDAS_OBSERVACOES_ADICIONAIS}          id:observacao
${PAINEL_DE_POS_VENDAS_PEDIDO_DE_VENDA}                 id:numeroOrder
${PAINEL_DE_POS_VENDAS_ICONE_PEDIDO_DE_VENDA}           id:iconeBuscarOrder
${PAINEL_DE_POS_VENDAS_NOME_ENTREGADOR}                 id:nomeEntregador
${PAINEL_DE_POS_VENDAS_DOCUMENTO}                       id:documento
${PAINEL_DE_POS_VENDAS_PLACA}                           id:placaVeiculo
${PAINEL_DE_POS_VENDAS_BTN_SOLICITAR_COLETA}            id:btnSolicitarColeta
${PAINEL_DE_POS_VENDAS_BTN_CONFIRMAR_RETORNO_COLETA}    id:btnConfirmarRetornoColeta
${PAINEL_DE_POS_VENDAS_BTN_ENCERRAR_SOLICITACAO}        id:btnEncerrarSolicitacao
${PAINEL_DE_POS_VENDAS_BTN_CANCELAR_SOLICITACAO}        id:btnCancelarSolicitacao
${PAINEL_DE_POS_VENDAS_BTN_IMPRIMIR_ORDEM_COLETA}       id:btnImprimirOrdemColeta
${PAINEL_DE_POS_VENDAS_BTN_SALVAR_SOLICITACAO}          id:btnSalvarSolicitacao
${PAINEL_DE_POS_VENDAS_BTN_APROVAR}                     id:btnAprovar
${PAINEL_DE_POS_VENDAS_BTN_REPROVAR}                    id:btnReprovar

*** Keywords ***
#Caso de Teste 01
Quando usuário selecionar uma opção no comboBox Seller
    Sleep  5    
    Wait Until Element Is Visible  ${PAINEL_DE_POS_VENDAS_COMBO_SELLER}  30sec
    Click Element                  ${PAINEL_DE_POS_VENDAS_COMBO_SELLER}
    Sleep  2
    Wait Until Element Is Visible  css:.mat-select-panel-wrap
    ${seller}=                     Get text  css:.mat-select-panel-wrap
    Log                            ${seller}
    Click Element                  xpath://span[contains(.,'FASTSHOP')] 

Então a opção selecionada no comboBox Seller ficará evidente 
    ${selected}=                   Get Text  ${PAINEL_DE_POS_VENDAS_COMBO_SELLER}
    Should Be Equal                ${selected}  FASTSHOP 

#Caso de Teste 02
Quando usuário selecionar uma opção no comboBox Web Store
    Sleep  5
    Wait Until Element Is Visible  ${PAINEL_DE_POS_VENDAS_COMBO_SELLER}  30sec
    Click Element                  ${PAINEL_DE_POS_VENDAS_COMBO_SELLER}
    Sleep  4
    Wait Until Element Is Visible  css:.mat-select-panel-wrap
    Click Element                  xpath://span[contains(.,'ELECTROLUXQA')] 
    Sleep  7
    Wait Until Element Is Visible  ${PAINEL_DE_POS_VENDAS_COMBO_WEBSTORE}  30sec
    Click Element                  ${PAINEL_DE_POS_VENDAS_COMBO_WEBSTORE}
    Sleep  2
    Wait Until Element Is Visible  css:.mat-select-panel-wrap
    ${webstore}=                   Get text  css:.mat-select-panel-wrap
    Log                            ${webstore}
    Click Element                  xpath://span[contains(.,'MKT_ELECTROLUX')] 

Então a opção selecionada no comboBox Web Store ficará evidente 
    ${selected}=                   Get Text  ${PAINEL_DE_POS_VENDAS_COMBO_WEBSTORE}
    Should Be Equal                ${selected}  MKT_ELECTROLUX

#Caso de Teste 03
Quando usuário selecionar uma opção no comboBox Lojas
    Sleep  2
    Wait Until Element Is Visible  ${PAINEL_DE_POS_VENDAS_COMBO_LOJAS}  30sec
    Click Element                  ${PAINEL_DE_POS_VENDAS_COMBO_LOJAS}
    Sleep  2
    Wait Until Element Is Visible  css:.mat-select-panel-wrap
    ${lojas}=                      Get text  css:.mat-select-panel-wrap
    Log                            ${lojas}
    Click Element                  xpath://span[contains(.,'SAM1 - Samsung Shopping Paulista')] 

Então a opção selecionada no comboBox Lojas ficará evidente
    ${selected}=                   Get Text  ${PAINEL_DE_POS_VENDAS_COMBO_LOJAS}
    Should Be Equal                ${selected}  SAM1 - Samsung Shopping Paulista

#Caso de Teste 04
Quando usuário inputar Data Início
    Sleep  2
    ${DATE}                        Get Current Date  result_format=%d%m%y
    Set Test Variable              ${DATE}
    Sleep  2
    Wait Until Element Is Visible  ${PAINEL_DE_POS_VENDAS_DATA_INICIO}  30sec
    Input Text                     ${PAINEL_DE_POS_VENDAS_DATA_INICIO}  09032021

Então a Data Início inputada ficará evidente no textfield
    Sleep  2
    Textfield Should Contain       ${PAINEL_DE_POS_VENDAS_DATA_INICIO}  2021-03-09

#Caso de Teste 05
Quando usuário inputar Data Fim
    Sleep  2
    ${DATE}                        Get Current Date  result_format=%d%m%y
    Set Test Variable              ${DATE}
    Sleep  2
    Wait Until Element Is Visible  ${PAINEL_DE_POS_VENDAS_DATA_FIM}  30sec
    Input Text                     ${PAINEL_DE_POS_VENDAS_DATA_FIM}  09032021

Então a Data Fim inputada ficará evidente no textfield
    Sleep  2
    Textfield Should Contain       ${PAINEL_DE_POS_VENDAS_DATA_FIM}  2021-03-09

#Caso de Teste 06
Quando usuário selecionar uma opção no comboBox Status
    Sleep  2
    Wait Until Element Is Visible  ${PAINEL_DE_POS_VENDAS_COMBO_STATUS}  30sec
    Click Element                  ${PAINEL_DE_POS_VENDAS_COMBO_STATUS}
    Sleep  2
    Wait Until Element Is Visible  css:.mat-select-panel-wrap
    ${status}=                     Get text  css:.mat-select-panel-wrap
    Log                            ${Status}
    Click Element                  xpath://span[contains(.,'Finalizada')] 

Então a opção selecionada no comboBox Status ficará evidente
    ${selected}=                   Get Text  ${PAINEL_DE_POS_VENDAS_COMBO_STATUS}
    Should Be Equal                ${selected}  Finalizada

#Caso de Teste 07
Quando usuário clicar no botão Atualizar
    Sleep  2
    Wait Until Element Is Visible  ${PAINEL_DE_POS_VENDAS_BTN_ATUALIZAR}  30sec 
    Click Element                  ${PAINEL_DE_POS_VENDAS_BTN_ATUALIZAR}

Então usuário irá consegui atualizar filtro de pedidos
    Sleep  5
    Page Should Contain Element    ${PAINEL_DE_POS_VENDAS_TABLE}

#Caso de Teste 08
Quando usuário clicar no botão Buscar
    Sleep  2
    Wait Until Element Is Visible  ${PAINEL_DE_POS_VENDAS_BTN_BUSCAR}  30sec 
    Click Element                  ${PAINEL_DE_POS_VENDAS_BTN_BUSCAR} 

Então usuário irá conseguir visualizar modal Filtros
    Sleep  2
    Page Should Contain        Filtros

#Caso de Teste 09
Quando usuário clicar no botão Buscar e digitar no textfield Número da Solicitação 
    Sleep  2
    Wait Until Element Is Visible  ${PAINEL_DE_POS_VENDAS_BTN_BUSCAR}  30sec 
    Click Element                  ${PAINEL_DE_POS_VENDAS_BTN_BUSCAR} 
    Sleep  2
    ${numero_solicitacao}=         generate random string  4  [NUMBERS]
    Set Test Variable              ${numero_solicitacao}
    Wait Until Element Is Visible  ${PAINEL_DE_POS_VENDAS_NUMERO_SOLICITACAO}  30sec 
    Input Text                     ${PAINEL_DE_POS_VENDAS_NUMERO_SOLICITACAO}  ${numero_solicitacao} 

Então o Número da Solicitação inputada ficará evidente no textfield
    Sleep  2
    Textfield Should Contain       ${PAINEL_DE_POS_VENDAS_NUMERO_SOLICITACAO}  ${numero_solicitacao} 

#Caso de Teste 10
Quando usuário clicar no botão Buscar e digitar no textfield Número do Pedido 
    Sleep  2
    Wait Until Element Is Visible  ${PAINEL_DE_POS_VENDAS_BTN_BUSCAR}  30sec 
    Click Element                  ${PAINEL_DE_POS_VENDAS_BTN_BUSCAR} 
    Sleep  2
    ${numero_pedido}=              generate random string  4  [NUMBERS]
    Set Test Variable              ${numero_pedido}
    Wait Until Element Is Visible  ${PAINEL_DE_POS_VENDAS_NUMERO_PEDIDO}  30sec 
    Input Text                     ${PAINEL_DE_POS_VENDAS_NUMERO_PEDIDO}  ${numero_pedido}

Então o Número do Pedido inputada ficará evidente no textfield
    Sleep  2
    Textfield Should Contain       ${PAINEL_DE_POS_VENDAS_NUMERO_PEDIDO}  ${numero_pedido}

#Caso de Teste 11
Quando usuário clicar no botão Buscar e digitar no textfield Nome de Cliente
    Sleep  2
    Wait Until Element Is Visible  ${PAINEL_DE_POS_VENDAS_BTN_BUSCAR}  30sec 
    Click Element                  ${PAINEL_DE_POS_VENDAS_BTN_BUSCAR}
    Sleep  2
    ${NOME}                        FakerLibrary.Name
    Set Test Variable              ${NOME}
    Wait Until Element Is Visible  ${PAINEL_DE_POS_VENDAS_NOME_CLIENTE}  30sec 
    Input Text                     ${PAINEL_DE_POS_VENDAS_NOME_CLIENTE}  ${NOME}

Então o Nome de Cliente inputada ficará evidente no textfield
    Textfield Should Contain       ${PAINEL_DE_POS_VENDAS_NOME_CLIENTE}  ${NOME}

#Caso de Teste 12
Quando usuário clicar no botão Buscar, preencher os campos e em seguida clicar no botão Limpar
    Sleep  2
    Wait Until Element Is Visible  ${PAINEL_DE_POS_VENDAS_BTN_BUSCAR}  30sec 
    Click Element                  ${PAINEL_DE_POS_VENDAS_BTN_BUSCAR} 
    Sleep  2
    ${numero_solicitacao}=         generate random string  4  [NUMBERS]
    Set Test Variable              ${numero_solicitacao}
    Wait Until Element Is Visible  ${PAINEL_DE_POS_VENDAS_NUMERO_SOLICITACAO}  30sec 
    Input Text                     ${PAINEL_DE_POS_VENDAS_NUMERO_SOLICITACAO}  ${numero_solicitacao} 
    Sleep  2
    ${numero_pedido}=              generate random string  4  [NUMBERS]
    Set Test Variable              ${numero_pedido}
    Wait Until Element Is Visible  ${PAINEL_DE_POS_VENDAS_NUMERO_PEDIDO}  30sec 
    Input Text                     ${PAINEL_DE_POS_VENDAS_NUMERO_PEDIDO}  ${numero_pedido}
    Sleep  2
    ${NOME}                        FakerLibrary.Name
    Set Test Variable              ${NOME}
    Wait Until Element Is Visible  ${PAINEL_DE_POS_VENDAS_NOME_CLIENTE}  30sec 
    Input Text                     ${PAINEL_DE_POS_VENDAS_NOME_CLIENTE}  ${NOME}
    Sleep  2
    Click Element                  ${PAINEL_DE_POS_VENDAS_BTN_LIMPAR}

Então usuário conseguirá limpar dados digitado nos campos
    Textfield Should Contain       ${PAINEL_DE_POS_VENDAS_NUMERO_SOLICITACAO}  ${EMPTY}
    Textfield Should Contain       ${PAINEL_DE_POS_VENDAS_NUMERO_PEDIDO}       ${EMPTY}
    Textfield Should Contain       ${PAINEL_DE_POS_VENDAS_NOME_CLIENTE}        ${EMPTY}

#Caso de Teste 13
Quando usuário clicar no botão Buscar em seguida no ícone de fechar (X)
    Sleep  2
    Wait Until Element Is Visible  ${PAINEL_DE_POS_VENDAS_BTN_BUSCAR}  30sec 
    Click Element                  ${PAINEL_DE_POS_VENDAS_BTN_BUSCAR} 
    Sleep  2
    Wait Until Element Is Visible  ${PAINEL_DE_POS_VENDAS_FECHAR}  30sec 
    Click Element                  ${PAINEL_DE_POS_VENDAS_FECHAR} 

Então usuário irá conseguir fechar a modal Filtros
    Sleep  2
    Page Should Not Contain        Filtros

#Caso de Teste 14
Quando usuário clicar no botão Buscar e efetuar uma buscar por Número da Solicitação inconsistente
    Sleep  2
    Wait Until Element Is Visible  ${PAINEL_DE_POS_VENDAS_BTN_BUSCAR}  30sec 
    Click Element                  ${PAINEL_DE_POS_VENDAS_BTN_BUSCAR} 
    Sleep  2
    ${numero_solicitacao}=         generate random string  4  [NUMBERS]
    Set Test Variable              ${numero_solicitacao}
    Wait Until Element Is Visible  ${PAINEL_DE_POS_VENDAS_NUMERO_SOLICITACAO}  30sec 
    Input Text                     ${PAINEL_DE_POS_VENDAS_NUMERO_SOLICITACAO}  ${numero_solicitacao} 
    Sleep  2
    Click Element                  ${PAINEL_DE_POS_VENDAS_BTN_MODAL_BUSCAR}

Então usuário irá visualizar uma mensagem de alerta 
    Sleep  7
    Wait Until Element Is Visible  xpath://span[contains(.,'ERRO')]
    Page Should Contain Element    xpath://span[contains(.,'ERRO')]

#Caso de Teste 15
Quando usuário clicar no botão Buscar e efetuar uma buscar por Número de Pedido inconsistente
    Sleep  2
    Wait Until Element Is Visible  ${PAINEL_DE_POS_VENDAS_BTN_BUSCAR}  30sec 
    Click Element                  ${PAINEL_DE_POS_VENDAS_BTN_BUSCAR} 
    Sleep  2
    ${numero_pedido}=              generate random string  4  [NUMBERS]
    Set Test Variable              ${numero_pedido}
    Wait Until Element Is Visible  ${PAINEL_DE_POS_VENDAS_NUMERO_PEDIDO}  30sec 
    Input Text                     ${PAINEL_DE_POS_VENDAS_NUMERO_PEDIDO}  ${numero_pedido}
    Sleep  2
    Click Element                  ${PAINEL_DE_POS_VENDAS_BTN_MODAL_BUSCAR}

#Caso de Teste 16
Quando usuário clicar no botão Buscar e efetuar uma buscar por Nome de Cliente inconsistente
    Sleep  2
    Wait Until Element Is Visible  ${PAINEL_DE_POS_VENDAS_BTN_BUSCAR}  30sec 
    Click Element                  ${PAINEL_DE_POS_VENDAS_BTN_BUSCAR}
    Sleep  2
    ${NOME}                        FakerLibrary.Name
    Set Test Variable              ${NOME}
    Wait Until Element Is Visible  ${PAINEL_DE_POS_VENDAS_NOME_CLIENTE}  30sec 
    Input Text                     ${PAINEL_DE_POS_VENDAS_NOME_CLIENTE}  ${NOME}
    Sleep  2
    Click Element                  ${PAINEL_DE_POS_VENDAS_BTN_MODAL_BUSCAR}

#Caso de Teste 17
Quando usuário clicar no botão Abrir Nova Solicitação
    Sleep  2
    Wait Until Element Is Visible  ${PAINEL_DE_POS_VENDAS_BTN_ABRIR_NOVA_SOLICITACAO}  30sec 
    Click Element                  ${PAINEL_DE_POS_VENDAS_BTN_ABRIR_NOVA_SOLICITACAO}
    Sleep  2

Então usuário será direcionado para a página de Solicitação Troca/Cancelamento
    Page Should Contain            Solicitação Troca Cancelamento

#Caso de Teste 18
Quando usuário acessar a página de Solicitação Troca/Cancelamento e selecionar uma opção no comboBox Tipo Solicitação
    Sleep  5    
    Wait Until Element Is Visible  ${PAINEL_DE_POS_VENDAS_BTN_ABRIR_NOVA_SOLICITACAO}  30sec 
    Click Element                  ${PAINEL_DE_POS_VENDAS_BTN_ABRIR_NOVA_SOLICITACAO}
    Sleep  2
    Wait Until Element Is Visible  ${PAINEL_DE_POS_VENDAS_COMBO_TIPO_SOLICITACAO}  30sec
    Click Element                  ${PAINEL_DE_POS_VENDAS_COMBO_TIPO_SOLICITACAO}
    Sleep  2
    Wait Until Element Is Visible  css:.mat-select-panel-wrap
    ${tipo_solicitacao}=           Get text  css:.mat-select-panel-wrap
    Log                            ${tipo_solicitacao}
    Click Element                  xpath://span[contains(.,'Troca')] 

Então a opção selecionada no comboBox Tipo Solicitação ficará evidente 
    ${selected}=                   Get Text  ${PAINEL_DE_POS_VENDAS_COMBO_TIPO_SOLICITACAO}
    Should Be Equal                ${selected}  Troca 

#Caso de Teste 19
Quando usuário acessar a página de Solicitação Troca/Cancelamento e selecionar uma opção no comboBox Motivo
    Sleep  5    
    Wait Until Element Is Visible  ${PAINEL_DE_POS_VENDAS_BTN_ABRIR_NOVA_SOLICITACAO}  30sec 
    Click Element                  ${PAINEL_DE_POS_VENDAS_BTN_ABRIR_NOVA_SOLICITACAO}
    Sleep  2
    Wait Until Element Is Visible  ${PAINEL_DE_POS_VENDAS_COMBO_MOTIVO}  30sec
    Click Element                  ${PAINEL_DE_POS_VENDAS_COMBO_MOTIVO}
    Sleep  2
    Wait Until Element Is Visible  css:.mat-select-panel-wrap
    ${motivo}=                     Get text  css:.mat-select-panel-wrap
    Log                            ${motivo}
    Click Element                  xpath://span[contains(.,'Defeito')] 

Então a opção selecionada no comboBox Motivo ficará evidente 
    ${selected}=                   Get Text  ${PAINEL_DE_POS_VENDAS_COMBO_MOTIVO}
    Should Be Equal                ${selected}  Defeito 

#Caso de Teste 20
Quando usuário acessar a página de Solicitação Troca/Cancelamento e inputar dados no textarea Observações adicionais
    Sleep  5    
    Wait Until Element Is Visible  ${PAINEL_DE_POS_VENDAS_BTN_ABRIR_NOVA_SOLICITACAO}  30sec 
    Click Element                  ${PAINEL_DE_POS_VENDAS_BTN_ABRIR_NOVA_SOLICITACAO}
    Sleep  2
    Wait Until Element Is Visible  ${PAINEL_DE_POS_VENDAS_OBSERVACOES_ADICIONAIS}  30sec
    Input Text                     ${PAINEL_DE_POS_VENDAS_OBSERVACOES_ADICIONAIS}  Test
    Sleep  2

Então as Observações adicionais inputada ficará evidente no textarea 
    Textarea Should Contain       ${PAINEL_DE_POS_VENDAS_OBSERVACOES_ADICIONAIS}  Test

#Caso de Teste 21
Quando usuário acessar a página de Solicitação Troca/Cancelamento e inputar dados no textfield Pedido de Venda
    Sleep  5    
    Wait Until Element Is Visible  ${PAINEL_DE_POS_VENDAS_BTN_ABRIR_NOVA_SOLICITACAO}  30sec 
    Click Element                  ${PAINEL_DE_POS_VENDAS_BTN_ABRIR_NOVA_SOLICITACAO}
    Sleep  2
    Wait Until Element Is Visible  ${PAINEL_DE_POS_VENDAS_PEDIDO_DE_VENDA}  30sec
    Input Text                     ${PAINEL_DE_POS_VENDAS_PEDIDO_DE_VENDA}  Test
    Sleep  2

Então o Pedido de Venda inputada ficará evidente no textfield
    Textfield Should Contain       ${PAINEL_DE_POS_VENDAS_PEDIDO_DE_VENDA}  Test

#Caso de Teste 22
Quando usuário acessar a página de Solicitação Troca/Cancelamento e inputar dados no textfield Pedido de Venda em seguida clicar no ícone de pesquisa
    Sleep  5    
    Wait Until Element Is Visible  ${PAINEL_DE_POS_VENDAS_BTN_ABRIR_NOVA_SOLICITACAO}  30sec 
    Click Element                  ${PAINEL_DE_POS_VENDAS_BTN_ABRIR_NOVA_SOLICITACAO}
    Sleep  2
    Wait Until Element Is Visible  ${PAINEL_DE_POS_VENDAS_PEDIDO_DE_VENDA}  30sec
    Input Text                     ${PAINEL_DE_POS_VENDAS_PEDIDO_DE_VENDA}  CM2001393
    Sleep  2
    Click Element                  ${PAINEL_DE_POS_VENDAS_ICONE_PEDIDO_DE_VENDA}  
    Sleep  2

Então usuário irá consegui filtrar um Pedido de Venda com sucesso
    Textfield Should Contain       ${PAINEL_DE_POS_VENDAS_PEDIDO_DE_VENDA}  CM2001393

#Caso de Teste 23
Quando usuário acessar a página de Solicitação Troca/Cancelamento e inputar dados inconsistente no textfield Pedido de Venda em seguida clicar no ícone de pesquisa
    Sleep  5    
    Wait Until Element Is Visible  ${PAINEL_DE_POS_VENDAS_BTN_ABRIR_NOVA_SOLICITACAO}  30sec 
    Click Element                  ${PAINEL_DE_POS_VENDAS_BTN_ABRIR_NOVA_SOLICITACAO}
    Sleep  2
    Wait Until Element Is Visible  ${PAINEL_DE_POS_VENDAS_PEDIDO_DE_VENDA}  30sec
    Input Text                     ${PAINEL_DE_POS_VENDAS_PEDIDO_DE_VENDA}  Test
    Sleep  2
    Click Element                  ${PAINEL_DE_POS_VENDAS_ICONE_PEDIDO_DE_VENDA}  
    Sleep  2

#Caso de Teste 24
Quando usuário acessar a página de Solicitação Troca/Cancelamento e digitar no textfield Nome do Entregador
    Sleep  5    
    Wait Until Element Is Visible  ${PAINEL_DE_POS_VENDAS_BTN_ABRIR_NOVA_SOLICITACAO}  30sec 
    Click Element                  ${PAINEL_DE_POS_VENDAS_BTN_ABRIR_NOVA_SOLICITACAO}
    Sleep  2
    ${NOME}                        FakerLibrary.Name
    Set Test Variable              ${NOME}
    Wait Until Element Is Visible  ${PAINEL_DE_POS_VENDAS_NOME_ENTREGADOR}  30sec
    Input Text                     ${PAINEL_DE_POS_VENDAS_NOME_ENTREGADOR}  ${NOME}  
    Sleep  2

Então o Nome do Entregador inputada ficará evidente no textfield 
    Textfield Should Contain       ${PAINEL_DE_POS_VENDAS_NOME_ENTREGADOR}  ${NOME}

#Caso de Teste 25
Quando usuário acessar a página de Solicitação Troca/Cancelamento e digitar no textfield Documento
    Sleep  5    
    Wait Until Element Is Visible  ${PAINEL_DE_POS_VENDAS_BTN_ABRIR_NOVA_SOLICITACAO}  30sec 
    Click Element                  ${PAINEL_DE_POS_VENDAS_BTN_ABRIR_NOVA_SOLICITACAO}
    Sleep  2
    Wait Until Element Is Visible  ${PAINEL_DE_POS_VENDAS_DOCUMENTO}  30sec
    Input Text                     ${PAINEL_DE_POS_VENDAS_DOCUMENTO}  99999999999  
    Sleep  2

Então o Documento inputada ficará evidente no textfield 
    Textfield Should Contain       ${PAINEL_DE_POS_VENDAS_DOCUMENTO}  999.999.999-99

#Caso de Teste 26
Quando usuário acessar a página de Solicitação Troca/Cancelamento e digitar no textfield Placa de Veículo
    Sleep  5    
    Wait Until Element Is Visible  ${PAINEL_DE_POS_VENDAS_BTN_ABRIR_NOVA_SOLICITACAO}  30sec 
    Click Element                  ${PAINEL_DE_POS_VENDAS_BTN_ABRIR_NOVA_SOLICITACAO}
    Sleep  2
    Wait Until Element Is Visible  ${PAINEL_DE_POS_VENDAS_PLACA}  30sec
    Input Text                     ${PAINEL_DE_POS_VENDAS_PLACA}  DED9999
    Sleep  2

Então a Placa de Veículo inputada ficará evidente no textfield 
    Textfield Should Contain       ${PAINEL_DE_POS_VENDAS_PLACA}  DED-9999

#Caso de Teste 27
Quando usuário acessar a página de Solicitação Troca/Cancelamento com status Aguardando Solicitação Coleta
    Sleep  5    
    Wait Until Element Is Visible  ${PAINEL_DE_POS_VENDAS_BTN_ATUALIZAR}  30sec 
    Click Element                  ${PAINEL_DE_POS_VENDAS_BTN_ATUALIZAR}
    Sleep  2
    Wait Until Element Is Visible  xpath://td[contains(.,'Aguardando Solicitação Coleta')]  30sec
    Click Element                  xpath://td[contains(.,'Aguardando Solicitação Coleta')]  
    Sleep  2

Então o botão Solicitar Coleta deve apresentar habilitado para receber click 
    Sleep  2
    Element Should Be Enabled      ${PAINEL_DE_POS_VENDAS_BTN_SOLICITAR_COLETA}

#Caso de Teste 28
Quando usuário acessar a página de Solicitação Troca/Cancelamento com status Em Coleta
    Sleep  5    
    Wait Until Element Is Visible  ${PAINEL_DE_POS_VENDAS_BTN_ATUALIZAR}  30sec 
    Click Element                  ${PAINEL_DE_POS_VENDAS_BTN_ATUALIZAR}
    Sleep  2
    Wait Until Element Is Visible  xpath://td[contains(.,'Em Coleta')]  30sec
    Click Element                  xpath://td[contains(.,'Em Coleta')]  
    Sleep  2

Então o botão Confirmar Retorno da Coleta deve apresentar habilitado para receber click 
    Sleep  2
    Element Should Be Enabled      ${PAINEL_DE_POS_VENDAS_BTN_CONFIRMAR_RETORNO_COLETA}

#Caso de Teste 29
Quando usuário acessar a página de Solicitação Troca/Cancelamento com status Coleta Realizada
    Sleep  5    
    Wait Until Element Is Visible  ${PAINEL_DE_POS_VENDAS_BTN_ATUALIZAR}  30sec 
    Click Element                  ${PAINEL_DE_POS_VENDAS_BTN_ATUALIZAR}
    Sleep  2
    Wait Until Element Is Visible  xpath://td[contains(.,'Coleta Realizada')]  30sec
    Click Element                  xpath://td[contains(.,'Coleta Realizada')]  
    Sleep  2

Então o botão Encerrar Solicitação deve apresentar habilitado para receber click 
    Sleep  2
    Element Should Be Enabled      ${PAINEL_DE_POS_VENDAS_BTN_ENCERRAR_SOLICITACAO}

#Caso de Teste 30
Quando usuário acessar a página de Solicitação Troca/Cancelamento com status Em Aprovação Seller
    Sleep  5    
    Wait Until Element Is Visible  ${PAINEL_DE_POS_VENDAS_BTN_ATUALIZAR}  30sec 
    Click Element                  ${PAINEL_DE_POS_VENDAS_BTN_ATUALIZAR}
    Sleep  2
    Wait Until Element Is Visible  xpath://td[contains(.,'Em Aprovação Seller')]  30sec
    Click Element                  xpath://td[contains(.,'Em Aprovação Seller')]  
    Sleep  2

Então o botão Cancelar Solicitação deve apresentar habilitado para receber click
    Sleep  2
    Element Should Be Enabled      ${PAINEL_DE_POS_VENDAS_BTN_CANCELAR_SOLICITACAO}

#Caso de Teste 31
Então o botão Imprimir Ordem de Coleta deve apresentar habilitado para receber click 
    Sleep  2
    Element Should Be Enabled      ${PAINEL_DE_POS_VENDAS_BTN_IMPRIMIR_ORDEM_COLETA}

#Caso de Teste 32
Então o botão Salvar Solicitação deve apresentar habilitado para receber click 
    Sleep  2
    Run Keyword And Ignore Error   Element Should Be Enabled      ${PAINEL_DE_POS_VENDAS_BTN_SALVAR_SOLICITACAO}

#Caso de Teste 33
Então o botão Aprovar deve apresentar habilitado para receber click 
    Sleep  2
    Element Should Be Enabled      ${PAINEL_DE_POS_VENDAS_BTN_APROVAR}

#Caso de Teste 34
Então o botão Reprovar deve apresentar habilitado para receber click 
    Sleep  2
    Element Should Be Enabled      ${PAINEL_DE_POS_VENDAS_BTN_REPROVAR}






